<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\PerusahaanController;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    if(auth()->user())
        return redirect()->route('home');
    else
        return redirect()->route('login');
});

Auth::routes();

// Route::get('/home', function () {
//     return view('dashboard');
// });

Route::get('home', 'HomeController@index')->name('home');
Route::get('home/json', 'HomeController@json')->name('home.json');

Route::get('logout', 'HomeController@logout');

// Get Calendar
Route::get('calendar', 'FullCalendarController@index')->name('calendar');

// Route AddMember
Route::get('anggota','AddMemberController@index')->name('anggota');
Route::get('anggota/create','AddMemberController@create')->name('anggota.create');
Route::post('anggota/store', 'AddMemberController@store')->name('anggota.store');
Route::put('anggota/update/{id}', 'AddMemberController@update')->name('anggota.update');
Route::get('anggota/delete/{id}', 'AddMemberController@destroy')->name('anggota.destroy');
Route::get('anggota/search', 'AddMemberController@search')->name('anggota.search');
Route::get('anggota/get_row', 'AddMemberController@getJson')->name('anggota.getJson');
Route::get('anggota/anggota_json', 'AddMemberController@anggota_json')->name('anggota.anggota_json');

// Route Perusahaan
Route::get('perusahaan', 'PerusahaanController@index')->name('perusahaan');
Route::get('perusahaan/create', 'PerusahaanController@create')->name('perusahaan.create');
Route::post('perusahaan/store', 'PerusahaanController@store')->name('perusahaan.store');
Route::put('perusahaan/update/{id}', 'PerusahaanController@update')->name('perusahaan.update');
Route::get('perusahaan/delete/{id}', 'PerusahaanController@destroy')->name('perusahaan.destroy');
Route::get('perusahaan/search', 'PerusahaanController@search')->name('perusahaan.search');
Route::get('perusahaan/detail/{id}', 'PerusahaanController@show')->name('perusahaan.show');
Route::get('api/perusahaan/search', 'PerusahaanController@getPerusahaan')->name('api.perusahaan.search');
Route::get('perusahaan/get_row', 'PerusahaanController@getJson')->name('perusahaan.getJson');
Route::get('perusahaan/detail/{id}/contact_json','PerusahaanController@contact_json')->name('perusahaan.contact_json');
Route::get('perusahaan/detail/{id}/catatan_json', 'PerusahaanController@catatan_json')->name('perusahaan.catatan_json');
Route::get('perusahaan/json', 'PerusahaanController@json')->name('perusahaan.json');

// Route Catatan
Route::get('catatan', 'CatatanController@index')->name('catatan');
Route::get('catatan/create', 'CatatanController@create')->name('catatan.create');
Route::post('catatan/store', 'CatatanController@store')->name('catatan.store');
Route::put('catatan/update/{id}', 'CatatanController@update')->name('catatan.update');
Route::get('catatan/delete/{id}', 'CatatanController@destroy')->name('catatan.destroy');
Route::get('catatan/search', 'CatatanController@search')->name('catatan.search');
Route::get('catatan/get_row', 'CatatanController@getJson')->name('catatan.getJson');
Route::get('catatan/json', 'CatatanController@json')->name('catatan.json');

// Route Staff Perusahaan
Route::get('contact', 'CpPerusahaanController@index')->name('contact');
Route::get('contact/create', 'CpPerusahaanController@create')->name('contact.create');
Route::post('contact/store', 'CpPerusahaanController@store')->name('contact.store');
Route::put('contact/update/{id}', 'CpPerusahaanController@update')->name('contact.update');
Route::get('contact/search', 'CpPerusahaanController@search')->name('contact.search');
Route::get('contact/delete/{id}', 'CpPerusahaanController@destroy')->name('contact.destroy');
Route::get('contact/detail/{id}', 'CpPerusahaanController@show')->name('contact.show');
Route::get('contact/get_row', 'CpPerusahaanController@getJson')->name('contact.getJson');
Route::get('contact/detail/{id}/json', 'CpPerusahaanController@showJson')->name('contact.json');
Route::get('api/contact/search', 'CpPerusahaanController@getContact')->name('api.contact.search');
Route::get('contact/json', 'CpPerusahaanController@json')->name('contact.json');

// Route Profile
Route::get('profile', 'ProfileController@index')->name('profile');
Route::put('profile/update/{id}', 'ProfileController@update')->name('profile.update');

// Route EventPerusahaan
Route::get('event', 'EventPerusahaanController@index')->name('event');
Route::get('event/create', 'EventPerusahaanController@create')->name('event.create');
Route::post('event/store', 'EventPerusahaanController@store')->name('event.store');
Route::get('event/search', 'EventPerusahaanController@search')->name('event.search');
Route::put('event/update/{id}', 'EventPerusahaanController@update')->name('event.update');
Route::get('event/delete/{id}', 'EventPerusahaanController@destroy')->name('event.destroy');
Route::get('event/get_row', 'EventPerusahaanController@getJson')->name('event.getJson');
Route::get('event/json', 'EventPerusahaanController@json')->name('event.json');

// Route Invoice
Route::get('invoice', 'InvoiceController@index')->name('invoice');
Route::get('invoice/json', 'InvoiceController@json')->name('invoice.json');
Route::get('invoice/create', 'InvoiceController@create')->name('invoice.create');
Route::post('invoice/store', 'InvoiceController@store')->name('invoice.store');
Route::get('invoice/get_row', 'InvoiceController@getData')->name('invoice.getData');
Route::get('invoice/delete/{id}', 'InvoiceController@destroy')->name('invoice.destroy');
Route::get('invoice/get_table_from', 'InvoiceController@getDetail')->name('invoice.getDetail');
Route::put('invoice/update/{id}', 'InvoiceController@update')->name('invoice.update');
Route::post('invoice/cetak/{id}', 'InvoiceController@cetakInvoice')->name('invoice.print');
Route::post('invoice/import', 'InvoiceController@import')->name('invoice.import');
Route::get('invoice/tag-filter', 'InvoiceController@filterByTag')->name('invoice.tag-filter');
Route::get('invoice/print_all', 'InvoiceController@cetakAll')->name('invoice.printAll');
Route::get('invoice/template', function(){
    return response()->download(storage_path('template_import.xlsx'));
})->name('invoice.template');