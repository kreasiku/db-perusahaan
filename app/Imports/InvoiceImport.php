<?php

namespace App\Imports;

use App\Invoice;
use App\CpPerusahaan;
use App\Perusahaan;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;

class InvoiceImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $header = array("Keterangan", "Perusahaan", "Kontak", "Tanggal", "Tenggat", "Tag", "Nomor Invoice");
        $item = array();
        $total = 0;
        if(array_slice($row,0,count($header)) !== $header){
            $import = new Invoice([
                'keterangan' => $row[0],
                'tanggal' => $row[3],
                'tenggat' => $row[4],
                'user' => Auth::user()->id,
                'invoice' => $row[6],
                'total' => $row[7]
            ]);
            
            for ($i=7; $i < count($row)-3; $i+=4) { 
                if($row[$i] !== null){
                    $set = array("nama" => $row[$i]);
                    $set["satuan"] = $row[$i+1];
                    $set["jumlah"] = $row[$i+2];
                    $set["harga"] = $row[$i+3];
                    $set["sub_total"] = intval($row[$i+3]) * intval($row[$i+2]);
                    $total += $set["sub_total"];

                    array_push($item, $set);
                }
            }

            $import->item_set = $item;
            $import->total = $total;

            $cp = CpPerusahaan::select('id','nama','no_hp','perusahaans')->where('nama', $row[2])->with('perusahaan:id,nama')->get();
            if($cp){
                foreach ($cp as $c) {
                    if($c->perusahaans){
                        $import->cp = $c->id;
                        $import->perusahaans = $c->perusahaans;
                    }
                }
            } else {
                $perusahaan = Perusahaan::select('id', 'nama')->where('nama', $row[1])->get();
                foreach ($perusahaan as $p) {
                    $import->perusahaans = $p->id;
                }
            }

            $import->save();

            if (!$import->invoice) {
                if (!$import->tanggal) {
                    $inv_date = date("m/Y");
                } else {
                    $inv_date = date("m/Y", strtotime($import->tanggal));
                }
                $import->invoice = "{$import->id}/SINV/{$inv_date}";
            }

            $tags = explode(",", $row[5]);
            $import->tag($tags);

            return $import;
        } else
            return null;
    }
}
