<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
    protected $table = 'perusahaans';

    protected $fillable = [
        'nama',
        'bidang',
        'deskripsi',
        'tanggal_berdiri',
        'user'
    ];

    function catatan(){
        return $this->hasMany('App\Catatan', 'perusahaans', 'id');
    }

    function invoice()
    {
        return $this->hasMany('App\Invoice', 'perusahaans');
    }

    function contact(){
        return $this->hasMany('App\CpPerusahaan', 'perusahaans', 'id');
    }

    function user()
    {
        return $this->belongsTo('App\User', 'user', 'id');
    }
}
