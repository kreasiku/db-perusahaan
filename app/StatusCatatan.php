<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusCatatan extends Model
{
    protected $fillable = [
        'status'
    ];

    function catatan(){
        return $this->hasMany('App\Catatan', 'status', 'id');
    }
}
