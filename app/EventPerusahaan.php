<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPerusahaan extends Model
{
    protected $fillable = [
        'nama',
        'perusahaans',
        'tanggal',
        'keterangan',
        'user'
    ];

    function perusahaan(){
        return $this->belongsTo('App\Perusahaan', 'perusahaans', 'id');
    }
}
