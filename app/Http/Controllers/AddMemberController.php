<?php

namespace App\Http\Controllers;

use App\User;
use App\Catatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use DataTables;

class AddMemberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('is_admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->is_admin == 1) {
            return view('anggota/index');
        } else
            return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('anggota/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|min:8',
            'divisi' => 'required',
        ]);
        // $show = AddMemberController::create($validatedData);

        $member = new User([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'divisi' => $request->get('divisi')
        ]);
        if(auth()->user()->is_admin == 1) {
            $saved = $member->save();
            if ($saved) {
                return redirect('anggota')->with('success', 'Anggota berhasil ditambahkan!');
            } else {
                return redirect('anggota')->with('failed', 'Gagal menambahkan anggota!');
            }
        } else {
            return redirect('anggota')->with('failed', 'Gagal menambahkan anggota!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'divisi' => 'required',
        ]);
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->is_admin = $request->is_admin;
        if ($request->password) {
            $user->password = $request->password;
        }
        $user->divisi = $request->divisi;

        if ($user->id == $request->get('uid')) {
            $saved = $user->save();

            if ($saved) {
                return redirect('anggota')->with(['success' => 'Data anggota berhasil diubah!']);
            } else {
                return redirect('anggota')->with(['failed' => 'Gagal mengubah data anggota!']);
            }
        } else {
            return redirect('anggota')->with(['failed' => 'Kamu tidak diizinkan mengubah anggota ini!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $member = User::find($id);
        Response::json($request);
        if ((auth()->user()->is_admin === 1 && $request->is_admin === 0) && $request->id == $id) {
            $deleted = $member->delete();
            if ($deleted) {
                return redirect('anggota')->with(['success' => 'Data Anggota dihapus!']);
            } else {
                return redirect('anggota')->with(['failed' => 'Gagal menghapus Anggota!']);
            }
        } else
            return redirect('anggota')->with(['failed' => 'Anda tidak diizinkan menghapus Anggota!']);

    }

    public function search(Request $request)
    {
        $q = $request->get('q');
        $user = User::where('name', 'LIKE', '%' . $q . '%')->orWhere('divisi', 'LIKE', '%' . $q . '%')->paginate(10);
        if (count($user) > 0) {
            return view('anggota/index', compact('user'));
        } else {
            return view('anggota/index', compact('user'));
        }
    }

    public function getJson(Request $request){
        if(request()->ajax()){
            $anggota = User::find($request->id);

            $anggota['count'] = Catatan::where('user', $anggota['id'])->count();
            return Response::json($anggota);
        }
    }

    public function anggota_json()
    {
        $user = User::select('id','name','divisi','is_admin','created_at')->where('id','<>',auth()->user()->id)->get();
        $dt = DataTables::of($user);
        // return $dt;
        $dt->addColumn('aksi', function($data) {
            $result = '<button onclick="buttonClick('. $data->id .')" class="btn btn-success btn-sm catatan" data-toggle="modal" data-target=".view-anggota">
                <i class="fas fa-eye"></i> Lihat
            </button>
            <button onclick="buttonClick('. $data->id .')" class="btn btn-info btn-sm catatan" data-toggle="modal" data-target=".edit-anggota">
                <i class="fas fa-edit"></i> Ubah
            </button>';
            if($data->is_admin === 1) {
                $result = $result . ' <button disabled class="btn btn-danger btn-sm catatan">
                    <i class="fas fa-trash"></i> Hapus
                </button>';
            } else {
                $result = $result . ' <button onclick="buttonClick('. $data->id .')" class="btn btn-danger btn-sm catatan" data-toggle="modal" data-target=".delete-anggota">
                    <i class="fas fa-trash"></i> Hapus
                </button>';
            }

            return $result;
        });
        return $dt->rawColumns(['aksi'])->make(true);
    }
}
