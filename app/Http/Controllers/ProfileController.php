<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perusahaan;
use App\User;

use Response;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('profile/index');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'divisi' => 'required',
            'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        
        if ($request->password) {
            $user->password = $request->password;
        }

        if($request->avatar){
            $file = $request->avatar;
            $file_name = $file->getClientOriginalName();
            $path = $file->storeAs('public', $file_name);
            $user->avatar = 'storage/' . $file_name;
        }

        $user->divisi = $request->divisi;
        $saved = $user->save();
        if ($user->id == $request->id) {
            $saved = $user->save();
            if ($saved) {
                return redirect('profile')->with(['success' => 'Data Profile berhasil diubah!']);
            } else {
                return redirect('profile')->with(['failed' => 'Gagal mengubah Data Pnggota!']);
            }
        } else {
            return redirect('profile')->with(['failed' => 'Kamu tidak diizinkan mengubah Profile ini!']);
        }
    }
}
