<?php

namespace App\Http\Controllers;

use App\CpPerusahaan;
use App\Perusahaan;
use Illuminate\Http\Request;
use Redirect,Response;
use Carbon\Carbon;

class FullCalendarController extends Controller
{
    public function index(){
        $cp = CpPerusahaan::select('nama as title', 'tgl_lahir as start')->whereNotNull('tgl_lahir');
        $perusahaan = Perusahaan::select('nama as title', 'tanggal_berdiri as start')->whereNotNull('tanggal_berdiri');

        if(request()->ajax()){
            $start = request()->start;
            $dateStart = explode("-", $start);
            $startMonth = $dateStart[1];
            $startYear = $dateStart[0];
            
            $end = request()->end;
            $dateEnd = explode("-", $end);
            $endMonth = $dateEnd[1];
            $endYear = $dateEnd[0];

            $cp = $cp->whereMonth('tgl_lahir', '>=', $startMonth);
            $event = $perusahaan->whereMonth('tanggal_berdiri', '>=', $startMonth)->union($cp)->get();

            foreach ($event as $e) {
                $date = explode("-",$e['start']);
                $e['start'] = $startYear . "-" . $date[1] . "-" . $date[2];
                $e['end'] = $startYear . "-" . $date[1] . "-" . $date[2];
                $e['title'] .= " BirthDay";
                $e['allDay'] = true;
            }

            return Response::json($event);
            
        }
        
        return view('calendar');
    }
}
