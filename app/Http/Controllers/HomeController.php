<?php

namespace App\Http\Controllers;


use App\Catatan;
use App\EventPerusahaan;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    /**
     * Show the application user dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function json()
    {

        date_default_timezone_set("Asia/Jakarta");
        $currentDate = date("Y-m-d");
        $catatan = Catatan::select('id', 'perusahaans', 'catatans', 'status', 'cp', 'follow_up')
            ->with(['perusahaan:id,nama,tanggal_berdiri', 'contact:id,perusahaans,nama,no_hp,tgl_lahir', 'status:id,status'])
            ->where('status', '1')
            ->where('follow_up', $currentDate)
            ->where('user', Auth::user()->id);

        $dt = DataTables::of($catatan);

        $dt->addColumn('contacts', function ($data) {
            return $data->contact->nama . ' - ' . $data->contact->no_hp . '<br>' . $data->perusahaan->nama;
        });

        $dt->addColumn('action', function ($data) {
            $route = '<a class="btn btn-success btn-sm" href="/contact/detail/'.$data->cp.'"><i class="fas fa-eye"></i> Detail </a>';

            return $route;
        });

        return $dt->rawColumns(['contacts', 'action'])->make(true);
    }

    public function index()
    {
        $event = EventPerusahaan::get();

        return view('home', compact('event'));
    }

    /**
     * Show the application admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function logout()
    {
        session()->flush();
        return redirect('/');
    }
}
