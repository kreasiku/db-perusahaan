<?php

namespace App\Http\Controllers;

use App\Catatan;
use App\Perusahaan;
use App\CpPerusahaan;
use App\StatusCatatan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use DataTables;

class CatatanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function json(){
        $catatan = Catatan::select('id','perusahaans', 'cp', 'created_at', 'catatans', 'status', 'follow_up');
        $catatan->with(['contact:id,perusahaans,no_hp,nama', 'perusahaan:id,nama'])->where('user', Auth::user()->id);
        $dt = Datatables::of($catatan);

        $dt->addColumn('contact', function($data) {
            $result = "";
            if ($data->contact == NULL) {
                $result = "{$result} <i class='text-danger'>CP Tidak Tersedia</i> <br>";
            } else
                $result = "{$result} {$data->contact->nama} - {$data->contact->no_hp} <br>";

            if($data->perusahaans != null){
                $result = "{$result} {$data->perusahaan->nama}";
            } else{
                $result = "{$result} <i class='text-danger'>Perusahaan Tidak Tersedia</i> <br>";
            }

            return $result;
        });

        $dt->addColumn('aksi', function($data) {
            return '<button onclick="buttonClick('. $data->id .', '. $data->status .')" class="btn btn-success btn-sm catatan" data-toggle="modal" data-target=".view-catatan">
                <i class="fas fa-eye"></i> Lihat
            </button>
            <button onclick="buttonClick('. $data->id .', '. $data->status .')" class="btn btn-info btn-sm catatan" data-toggle="modal" data-target=".edit-catatan">
                <i class="fas fa-edit"></i> Ubah
            </button>
            <button onclick="buttonClick('. $data->id .', '. $data->status .')" class="btn btn-danger btn-sm catatan" data-toggle="modal" data-target=".delete-catatan">
                <i class="fas fa-trash"></i> Hapus
            </button>';
        });
        return $dt->editColumn('created_at', function ($catatan){
            return $catatan->created_at ? with(new Carbon($catatan->created_at))->format('Y-m-d') : '';
        })->editColumn('follow_up', function ($catatan) {
            return $catatan->follow_up ? with(new Carbon($catatan->follow_up))->format('Y-m-d') : '';
        })->rawColumns(['aksi','contact'])->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sc = StatusCatatan::all();
        return view('catatan/index', compact('sc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sc = StatusCatatan::all();

        return view('catatan/create', compact('sc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'perusahaans' => 'required',
            'catatans' => 'required',
            'status' => 'required',
            'user' => 'required',
            'cp' => 'required'
        ]);

        $catatan = new Catatan([
            'perusahaans' => $request->get('perusahaans'),
            'catatans' => $request->get('catatans'),
            'status' => $request->get('status'),
            'follow_up' => $request->get('follow_up'),
            'cp' => $request->get('cp'),
            'user' => $request->get('user'),
        ]);

        if ($catatan->user != Auth::user()->id) {
            return redirect('catatan')->with('failed', 'Anda tidak diperbolehkan menambah catatan untuk user lain!');
        } else {
            $saved = $catatan->save();
            if ($saved) {
                return redirect('catatan')->with('success', 'Catatan berhasil ditambahkan!');
            } else {
                return redirect('catatan')->with('failed', 'Gagal menambahkan catatan!');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // code
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'catatans' => 'required',
            'status' => 'required',
            'user' => 'required'
        ]);

        $catatan = Catatan::find($id);
        $catatan->catatans = $request->catatans;
        $catatan->status = $request->status;
        $catatan->follow_up = $request->follow_up;

        if ($catatan->user == Auth::user()->id && $catatan->id == $request->get('form-id')) {
            $saved = $catatan->save();

            if ($saved) {
                return redirect('catatan')->with(['success' => 'Data catatan berhasil diubah!']);
            } else {
                return redirect('catatan')->with(['failed' => 'Gagal mengubah data catatan!']);
            }
        } else {
            return redirect('catatan')->with(['failed' => 'Kamu tidak diizinkan mengubah catatan ini!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $catatan = Catatan::find($id);

        if (($catatan->user == auth()->user()->id) && $id == $request->id) {
            $deleted = $catatan->delete();

            if ($deleted) {
                return redirect('catatan')->with(['success' => 'Data catatan berhasil dihapus!']);
            } else {
                return redirect('catatan')->with(['failed' => 'Gagal menghapus data catatan!']);
            }
        } else
            return redirect('catatan')->with(['failed' => 'Anda tidak diizinkan menghapus catatan!']);
    }

    public function search(Request $request)
    {
        // code
    }

    public function getJson(Request $request)
    {
        $catatan = Catatan::select('id','perusahaans', 'cp', 'updated_at', 'catatans', 'status', 'follow_up')
                ->with(['perusahaan:id,nama', 'contact:id,nama,perusahaans,no_hp','contact.perusahaan:id,nama']);

        $catatan = $catatan->find($request->id);

        return Response::json($catatan);
    }
}
