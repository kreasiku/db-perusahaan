<?php

namespace App\Http\Controllers;

use App\Catatan;
use App\User;
use App\CpPerusahaan;
use App\Perusahaan;
use App\StatusCatatan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as JavaScript;
use Response;
use DataTables;

class CpPerusahaanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact/index');
    }

    public function json(){
        $contact = CpPerusahaan::select('id', 'nama', 'perusahaans', 'email', 'tgl_lahir', 'created_at', 'no_hp')->with('perusahaan:id,nama')->get();
        $dt = Datatables::of($contact);

        $dt->addColumn('aksi', function($data) {
            return '<a class="btn btn-success btn-sm" href="/contact/detail/'. $data->id .'">
            <i class="fas fa-eye"></i> Lihat
        </a>
        <button class="btn btn-info btn-sm contact" onclick="buttonClick('. $data->id .')" data-toggle="modal" data-target=".edit-contact">
            <i class="fas fa-edit"></i> Ubah
        </button>
        <button class="btn btn-danger btn-sm contact" onclick="buttonClick('. $data->id .')" data-toggle="modal" data-target=".delete-contact">
            <i class="fas fa-trash"></i> Hapus
        </button>';
        });

        $dt->addColumn('perusahaans', function($data){
            if($data->perusahaan)
                return $data->perusahaan->nama;
            else
                return '<i class="text-danger">Perusahaan Tidak Tersedia</i>';
        });

        return $dt->rawColumns(['aksi', 'perusahaans'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'perusahaans' => 'required',
            'nama' => 'required',
            'no_hp' => 'required',
            'user' => 'required'
        ]);

        $contact = new CpPerusahaan([
            'perusahaans' => $request->get('perusahaans'),
            'nama' => $request->get('nama'),
            'no_hp' => $request->get('no_hp'),
            'email' => $request->get('email'),
            'tgl_lahir' => $request->get('tgl_lahir'),
            'user' => $request->get('user')
        ]);

        $saved = $contact->save();
        if ($saved) {
            return redirect('contact')->with('success', 'Staff berhasil ditambahkan!');
        } else {
            return redirect('contact')->with('failed', 'Gagal menambahkan staff!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sc = StatusCatatan::all();
        $contact = CpPerusahaan::select('id', 'nama', 'no_hp', 'perusahaans', 'email')->with('perusahaan:id,nama,deskripsi')->find($id);
        if(!$contact->perusahaans){
            $contact->perusahaan = "Tidak Tersedia";
        }
        return view('contact/show', compact(['contact', 'sc']));
    }

    public function showJson($id)
    {
        $catatan = Catatan::select('id', 'perusahaans', 'catatans', 'status', 'follow_up', 'user', 'cp', 'created_at');
        $catatan->with(['user:id,name'])->where('cp', $id)->where('user', Auth::user()->id)->get();
        $dt = Datatables::of($catatan);
        $dt->editColumn('follow_up', function ($data) {
            return $data->follow_up ? with(new Carbon($data->follow_up))->format('Y-m-d') : '-';
        });
        return $dt->rawColumns(['perusahaan.nama'])->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'perusahaans' => 'required',
            'nama' => 'required',
            'no_hp' => 'required',
        ]);

        $contact = CpPerusahaan::find($id);
        $contact->perusahaans = $request->get('perusahaans');
        $contact->nama = $request->get('nama');
        $contact->email = $request->get('email');
        $contact->no_hp = $request->get('no_hp');
        $contact->tgl_lahir = $request->get('tgl_lahir');

        $catatan = $contact->catatan;
        foreach ($catatan as $c) {
            $c->perusahaans = $request->perusahaans;
            $c->save();
        }

        if ($contact->id == $request->get('cid')) {
            $saved = $contact->save();

            if ($saved) {
                return redirect('contact')->with(['success' => 'Data staff berhasil diubah!']);
            } else {
                return redirect('contact')->with(['failed' => 'Gagal mengubah data staff!']);
            }
        } else {
            return redirect('contact')->with(['failed' => 'Kamu tidak diizinkan mengubah staff ini!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $contact = CpPerusahaan::find($id);
        if ($contact->user == auth()->user()->id && $request->id == $id) {
            $deleted = $contact->delete();
            if ($deleted) {
                return redirect('contact')->with(['success' => 'Data staff perusahaan dihapus!']);
            } else {
                return redirect('contact')->with(['failed' => 'Gagal menghapus staff perusahaan!']);
            }
        } else
            return redirect('contact')->with(['failed' => 'Anda tidak diizinkan menghapus staff perusahaan!']);
    }

    public function search(Request $request)
    {
      //
    }

    public function getContact(Request $request)
    {
        $contact = CpPerusahaan::join('perusahaans', 'cp_perusahaans.perusahaans', '=', 'perusahaans.id')->where('cp_perusahaans.nama', 'LIKE', '%' . $request->input('term', '') . '%')->orWhere('perusahaans.nama', 'LIKE', '%' . $request->input('term', '') . '%')->get(['cp_perusahaans.id as id', 'cp_perusahaans.perusahaans as title', 'cp_perusahaans.nama as text', 'cp_perusahaans.no_hp as cphp', 'perusahaans.nama as pnama']);

        foreach ($contact as $row) {
            $row->text = $row->text . " - " . $row->pnama;
        }

        return ['results' => $contact];
    }

    public function getJson(Request $request)
    {
        if (request()->ajax()) {
            $contact = CpPerusahaan::with('perusahaan')->find($request->id);

            if (!$contact['perusahaans']) {
                $contact['perusahaans'] = "Tidak Ada";
            }

            // if(!$contact['user']){
            //     $user = User::find($contact['user'])->name;
            //     $contact['user'] = $user;
            // } else {
            //     $contact['user'] = "Tidak Ada";
            // }

            return Response::json($contact);
        }
    }
}
