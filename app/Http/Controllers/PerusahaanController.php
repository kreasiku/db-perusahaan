<?php

namespace App\Http\Controllers;

use App\Perusahaan;
use App\Catatan;
use App\CpPerusahaan;
use App\StatusCatatan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use DataTables;

class PerusahaanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function json(){
        $i = 0;
        $perusahaan = Perusahaan::select('id','nama', 'bidang', 'deskripsi', 'tanggal_berdiri', 'created_at');
        $dt = Datatables::of($perusahaan);

        $dt->addColumn('aksi', function($data) {
            return '<a class="btn btn-success btn-sm" href="/perusahaan/detail/'. $data->id .'">
            <i class="fas fa-eye"></i> Lihat
        </a>
        <button class="btn btn-info btn-sm perusahaan" onclick="buttonClick('. $data->id .')" data-toggle="modal" data-target=".edit-perusahaan">
            <i class="fas fa-edit"></i> Ubah
        </button>
        <button class="btn btn-danger btn-sm perusahaan" onclick="buttonClick('. $data->id .')" data-toggle="modal" data-target=".delete-perusahaan" href="#">
            <i class="fas fa-trash"></i> Hapus
        </button>';
        });

        // $dt->addColumn('#', function (){
        //     return $i = $i + 1;
        // });

        return $dt->rawColumns(['aksi'])->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $perusahaan = Perusahaan::get();
        return view('perusahaan/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('perusahaan/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'bidang' => 'required',
            'deskripsi' => 'required'
        ]);

        $perusahaan = new Perusahaan([
            'nama' => $request->get('nama'),
            'bidang' => $request->get('bidang'),
            'deskripsi' => $request->get('deskripsi'),
            'tanggal_berdiri' => $request->get('tanggal_berdiri'),
            'user' => $request->get('user')
        ]);

        $saved = $perusahaan->save();

        if ($saved) {
            return redirect('perusahaan')->with('success', 'Perusahaan berhasil ditambahkan!');
        } else {
            return redirect('perusahaan')->with('failed', 'Gagal menambahkan perusahaan!');
        }
    }

    public function contact_json($id)
    {
        $details = CpPerusahaan::select('id', 'perusahaans', 'nama','no_hp','tgl_lahir')->with(['perusahaan:id,nama,deskripsi,tanggal_berdiri'])->where('perusahaans',$id);
        $dt = Datatables::of($details);
        return $dt->make(true);
    }

    public function catatan_json($id){
        $catatan = Catatan::select('id','perusahaans','cp','created_at','catatans','status','follow_up');
        $catatan->with(['contact:id,perusahaans,no_hp,nama', 'perusahaan:id,nama'])->where('perusahaans',$id)->where('user', Auth::user()->id);
        $dt = Datatables::of($catatan);
        $dt->addColumn('status', function($data) {
            if($data->status == 1 ) {
                return '<span class="badge badge-info">Sedang Diproses</span>';
            } else if ($data->status == 2) {
                return '<span class="badge badge-danger">Gagal</span>';
            } else {
                return '<span class="badge badge-success">Selesai</span>';
            }
        });
        return $dt->rawColumns(['status'])->make(true);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $perusahaan = Perusahaan::find($id);
        // $status = StatusCatatan::all();
        // $catatan = Catatan::where('perusahaans', '=', $id)->get();
        // $contact = CpPerusahaan::where('perusahaans', '=', $perusahaan['id'])->paginate(5);
        return view('perusahaan/show', compact(['perusahaan']));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'bidang' => 'required',
            'deskripsi' => 'required'
        ]);

        $perusahaan = Perusahaan::find($id);
        $perusahaan->nama = $request->get('nama');
        $perusahaan->bidang = $request->get('bidang');
        $perusahaan->deskripsi = $request->get('deskripsi');
        $perusahaan->tanggal_berdiri = $request->get('tanggal_berdiri');

        if ($perusahaan->id == $request->get('pid')) {
            $saved = $perusahaan->save();

            if ($saved) {
                return redirect('perusahaan')->with(['success' => 'Data perusahaan berhasil diubah!']);
            } else {
                return redirect('perusahaan')->with(['failed' => 'Gagal mengubah data perusahaan!']);
            }
        } else {
            return redirect('perusahaan')->with(['failed' => 'Kamu tidak diizinkan mengubah perusahaan ini!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $perusahaan = Perusahaan::find($id);
        if ($request->id == $id) {
            $deleted = $perusahaan->delete();
            if ($deleted) {
                return redirect('perusahaan')->with(['success' => 'Data perusahaan berhasil dihapus!']);
            } else {
                return redirect('perusahaan')->with(['failed' => 'Gagal menghapus data perusahaan!']);
            }
        } else
            return redirect('perusahaan')->with(['failed' => 'Anda tidak diizinkan menghapus data perusahaan!']);
    }

    public function search(Request $request)
    {
        // code
    }

    public function getPerusahaan(Request $request)
    {
        $perusahaan = Perusahaan::where('nama', 'LIKE', '%' . $request->input('term', '') . '%')
            ->get(['id', 'nama as text']);
        return ['results' => $perusahaan];
    }

    public function getJson(Request $request){
        if(request()->ajax()){
            $perusahaan = Perusahaan::find($request->id);
            $perusahaan['count'] = Catatan::where('perusahaans', $request->id)->count();

            if($perusahaan['user']){
                $user = User::find($perusahaan['user'])->name;
                $perusahaan['user'] = $user;
            } else {
                $perusahaan['user'] = "Tidak Ada";
            }

            return Response::json($perusahaan);
        }
    }
}
