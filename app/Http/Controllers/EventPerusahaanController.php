<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventPerusahaan;
use App\Perusahaan;
use Illuminate\Support\Facades\Auth;
use Response;
use DataTables;

class EventPerusahaanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('event_perusahaan/index');
    }

    public function json(){
        $event = EventPerusahaan::select('id', 'nama', 'perusahaans', 'tanggal', 'keterangan', 'created_at')->with('perusahaan:id,nama')->get();
        $dt = Datatables::of($event);

        $dt->addColumn('aksi', function($data) {
            return '<button class="btn btn-success btn-sm event" onclick="buttonClick('. $data->id .')" data-toggle="modal" data-target=".view-event">
            <i class="fas fa-eye"></i> Lihat
        </button>
        <button class="btn btn-info btn-sm event" onclick="buttonClick('. $data->id .')" data-toggle="modal" data-target=".edit-event">
            <i class="fas fa-edit"></i> Ubah
        </button>
        <button class="btn btn-danger btn-sm event" onclick="buttonClick('. $data->id .')" data-toggle="modal" data-target=".delete-event" href="#">
            <i class="fas fa-trash"></i> Hapus
        </button>';
        });

        $dt->addColumn('perusahaans', function($data){
            if($data->perusahaan)
                return $data->perusahaan->nama;
            else
                return '<i class="text-danger">Perusahaan Tidak Tersedia</i>';
        });

        return $dt->rawColumns(['aksi', 'perusahaans'])->make(true);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('event_perusahaan/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tanggal' => 'required',
            'perusahaans' => 'required',
            'keterangan' => 'required',
            'user' => 'required'
        ]);

        $event = new EventPerusahaan([
            'nama' => $request->get('nama'),
            'tanggal' => $request->get('tanggal'),
            'perusahaans' => $request->get('perusahaans'),
            'keterangan' => $request->get('keterangan'),
            'user' => $request->get('user')
        ]);

        $saved = $event->save();

        if ($saved) {
            return redirect('event')->with('success', 'Event berhasil ditambahkan!');
        } else {
            return redirect('event')->with('failed', 'Gagal menambahkan event!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'tanggal' => 'required',
            'perusahaans' => 'required',
            'keterangan' => 'required',
            'user' => 'required'
        ]);

        $event = EventPerusahaan::find($id);
        $event->nama = $request->get('nama');
        $event->tanggal = $request->get('tanggal');
        $event->keterangan = $request->get('keterangan');
        $event->user = $request->get('user');

        if ($event->id == $request->get('eid')) {
            $saved = $event->save();

            if ($saved) {
                return redirect('event')->with(['success' => 'Data event berhasil diubah!']);
            } else {
                return redirect('event')->with(['failed' => 'Gagal mengubah data event!']);
            }
        } else {
            return redirect('event')->with(['failed' => 'Kamu tidak diizinkan mengubah event ini!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $event = EventPerusahaan::find($id);
        if ($event->user == auth()->user()->id && $request->id == $id) {
            $deleted = $event->delete();
            if ($deleted) {
                return redirect('event')->with(['success' => 'Data event perusahaan dihapus!']);
            } else {
                return redirect('event')->with(['failed' => 'Gagal menghapus event perusahaan!']);
            }
        } else
            return redirect('event')->with(['failed' => 'Anda tidak diizinkan menghapus event perusahaan!']);
    }

    public function search(Request $request)
    {
       // code
    }

    public function getJson(Request $request){
        if(request()->ajax()){
            $event = EventPerusahaan::with('perusahaan')->find($request->id);

            // if($event['user']){
            //     $user = User::find($event['user'])->name;
            //     $event['user'] = $user;
            // } else {
            //     $event['user'] = "Tidak Ada";
            // }

            if(!$event['perusahaans']){
                $event['perusahaans'] = "Tidak Ada";
            }
            return Response::json($event);
        }
    }
}
