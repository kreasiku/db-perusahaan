<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Imports\InvoiceImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use Illuminate\Support\Facades\Redirect;
use Response;
use Madnest\Madzipper\Madzipper;
use NcJoes\OfficeConverter\OfficeConverter;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoiceA = Invoice::all();
        return view('invoice/index', compact('invoiceA'));
    }

    public function json()
    {
        $invoice = Invoice::select('id', 'perusahaans', 'cp', 'tanggal', 'tenggat', 'keterangan','invoice', 'item_set');
        $invoice->with(['contact:id,perusahaans,no_hp,nama', 'perusahaan:id,nama','tagged'])->where('user', Auth::user()->id);
        $dt = DataTables::of($invoice);

        $dt->addColumn('contact', function($data) {
            $result = "";
            if ($data->contact == NULL) {
                $result = "{$result} <i class='text-danger'>CP Tidak Tersedia</i> <br>";
            } else
                $result = "{$result} {$data->contact->nama} - {$data->contact->no_hp} <br>";

            if($data->perusahaans != null){
                $result = "{$result} {$data->perusahaan->nama}";
            } else{
                $result = "{$result} <i class='text-danger'>Perusahaan Tidak Tersedia</i> <br>";
            }

            return $result;
        });

        $dt->editColumn('keterangan', function($data) {
            $tags = '';
            foreach ($data->tagged as $tag) {
                $tags .= "<button formaction='/invoice/{$tag['tag_slug']}' class='btn btn-xs btn-warning tagging' id='tag-click' name='tag' value='{$tag['tag_slug']}'><b>{$tag['tag_slug']}</b></button>";
            }
            $keterangan = "{$data->keterangan} <br> $tags";
            return $keterangan;
        });

        $dt->addColumn('aksi', function($data) {
            return '<button onclick="buttonClick('. $data->id .')" class="btn btn-success btn-sm" data-toggle="modal" data-target=".view-invoice">
                <i class="fas fa-eye"></i> Lihat
            </button>
            <button onclick="buttonClick('. $data->id . ')" class="btn btn-info btn-sm" data-toggle="modal" data-target=".edit-invoice">
                <i class="fas fa-edit"></i> Ubah
            </button>
            <button onclick="buttonClick('. $data->id . ')" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete-invoice">
                <i class="fas fa-trash"></i> Hapus
            </button>';
        });

        return $dt->rawColumns(['aksi','contact','keterangan'])->make(true);
    }

    public function filterByTag(Request $request)
    {
        $invoice = Invoice::select('id', 'perusahaans', 'cp', 'tanggal', 'tenggat', 'keterangan', 'invoice', 'item_set')->with(['contact:id,perusahaans,no_hp,nama', 'perusahaan:id,nama'])->withAnyTag(["{$request->get('tag')}"]);
        $dt = Datatables::of($invoice);

        $dt->addColumn('contact', function ($data) {
            $result = "";
            if ($data->contact == NULL) {
                $result = "{$result} <i class='text-danger'>CP Tidak Tersedia</i> <br>";
            } else
                $result = "{$result} {$data->contact->nama} - {$data->contact->no_hp} <br>";

            if ($data->perusahaans != null) {
                $result = "{$result} {$data->perusahaan->nama}";
            } else {
                $result = "{$result} <i class='text-danger'>Perusahaan Tidak Tersedia</i> <br>";
            }

            return $result;
        });

        $dt->editColumn('keterangan', function ($data) {
            $tags = '';
            foreach ($data->tagged as $tag) {
                $tags .= "<button class='btn btn-xs btn-warning tagging' id='tag-click' name='tag-slug' value='{$tag['tag_slug']}'><b>{$tag['tag_slug']}</b></button>";
            }
            $keterangan = "{$data->keterangan} <br> $tags";
            return $keterangan;
        });

        $dt->addColumn('aksi', function ($data) {
            return '<button onclick="buttonClick(' . $data->id . ')" class="btn btn-success btn-sm" data-toggle="modal" data-target=".view-invoice">
                <i class="fas fa-eye"></i> Lihat
            </button>
            <button onclick="buttonClick(' . $data->id . ')" class="btn btn-info btn-sm" data-toggle="modal" data-target=".edit-invoice">
                <i class="fas fa-edit"></i> Ubah
            </button>
            <button onclick="buttonClick(' . $data->id . ')" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete-invoice">
                <i class="fas fa-trash"></i> Hapus
            </button>';
        });

        return $dt->rawColumns(['aksi', 'contact', 'keterangan'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('invoice/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = array();
        $total = 0;

        $request->validate([
            'user' => 'required',
            'keterangan' => 'required',
            'item_set' => 'required',
            'tags' => 'required',
        ]);

        $invoice = new Invoice([
            'perusahaans' => $request->get('perusahaans'),
            'cp' => $request->get('cp'),
            'user' => $request->get('user'),
            'invoice' => $request->get('invoice'),
            'keterangan' => $request->get('keterangan'),
            'tenggat' => $request->get('tenggat'),
        ]);

        if($request->tanggal){
            $invoice->tanggal = $request->tanggal;
        }

        if($request->item_set){
            foreach ($request->item_set as $n) {
                $x['nama'] = $n['nama'];
                $x['satuan'] = $n['satuan'];
                $x['jumlah'] = $n['jumlah'];
                $x['harga'] = $n['harga'];
                $x['sub_total'] = $n['jumlah']*$n['harga'];
                // $invoice->item_set[$i] = $x['nama'];
                array_push($item, $x);
                $total+=$x['sub_total'];
            }
            $invoice->item_set = $item;
            $invoice->total = $total;
        }

        if ($invoice->user != Auth::user()->id) {
            return redirect('invoice')->with('failed', 'Anda tidak diperbolehkan menambah invoice untuk user lain!');
        } else {
            $invoice->save();

            if (!$invoice->invoice) {
                if (!$invoice->tanggal) {
                    $inv_date = date("m/Y");
                } else {
                    $inv_date = date("m/Y", strtotime($invoice->tanggal));
                }
                $invoice->invoice = "{$invoice->id}/SINV/{$inv_date}";
            }

            $tags = explode(",", $request->tags);
            $invoice->tag($tags);
            $saved = $invoice->save();
            if ($saved) {
                return redirect('invoice')->with('success', 'Invoice berhasil ditambahkan!');
            } else {
                return redirect('invoice')->with('failed', 'Gagal menambahkan invoice!');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = array();
        $total = 0;

        $request->validate([
            'user' => 'required',
            'keterangan' => 'required',
            'item_set' => 'required',
        ]);
        $invoice = Invoice::find($id);
        $invoice->keterangan = $request->keterangan;
        $invoice->tenggat = $request->tenggat;
        if($request->tanggal){
            $invoice->tanggal = $request->tanggal;
        }

        // return dd($request->item_set);
        if($request->item_set){
            foreach ($request->item_set as $n) {
                $x['nama'] = $n['nama'];
                $x['satuan'] = $n['satuan'];
                $x['jumlah'] = $n['jumlah'];
                $x['harga'] = $n['harga'];
                $x['sub_total'] = $n['jumlah']*$n['harga'];
                // $invoice->item_set[$i] = $x['nama'];
                array_push($item, $x);
                $total+=$x['sub_total'];
            }
            $invoice->item_set = $item;
            $invoice->total = $total;
        }
        $invoice->invoice = $request->invoice;
        $tags = explode(",", $request->tags);
        $invoice->retag($tags);

        if ($invoice->user == Auth::user()->id && $invoice->id == $request->id) {
            $saved = $invoice->save();
            if ($saved) {
                return redirect('invoice')->with(['success' => 'Data invoice berhasil diubah!']);
            } else {
                return redirect('invoice')->with(['failed' => 'Gagal mengubah data invoice!']);
            }
        } else {
            return redirect('invoice')->with(['failed' => 'Kamu tidak diizinkan mengubah invoice ini!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $invoice = Invoice::find($id);

        if ($invoice->user == Auth::user()->id && $id == $request->id) {

            $deleted = $invoice->delete();

            if ($deleted) {
                return redirect('invoice')->with(['success' => 'Data invoice berhasil dihapus!']);
            } else {
                return redirect('invoice')->with(['failed' => 'Gagal menghapus data invoice!']);
            }
        } else {
            return redirect('invoice')->with(['failed' => 'Anda tidak diizinkan menghapus invoice!']);
        }
    }

    public function getData(Request $request)
    {
        $invoice = Invoice::select('id', 'perusahaans', 'cp', 'tanggal', 'tenggat', 'keterangan', 'invoice', 'total', 'item_set')->with(['contact:id,perusahaans,no_hp,nama', 'perusahaan:id,nama', 'tagged'])->where('user', Auth::user()->id);
        $invoice = $invoice->find($request->id);
        $tanggal = date('m/Y', strtotime($invoice->tanggal));
        // $invoice->nomor = "{$invoice->id}/SINV/{$tanggal}";
        $invoice->terbilang = terbilang($invoice->total) . " Rupiah";

        if(!$invoice->tenggat)
            $invoice->tenggat = '-';

        return Response::json($invoice);
    }

    public function getDetail(Request $request){
        $invoice = Invoice::select('item_set')->where('user', Auth::user()->id);
        return Response::json($invoice->item_set);
    }

    public function cetakInvoice(Request $request)
    {

        $url = url()->previous();
        $item = array();
        $x = 1;
        $invoice = Invoice::select('id', 'perusahaans', 'cp', 'tanggal', 'tenggat', 'keterangan', 'total', 'item_set', 'invoice')->with(['contact:id,perusahaans,no_hp,nama', 'perusahaan:id,nama'])->where('user', Auth::user()->id);
        $invoice = $invoice->find($request->id);
        $invoice->nomor = $invoice->invoice;
        $invoice->terbilang = terbilang($invoice->total) . " rupiah";
        $name = str_replace('/','_',$invoice->invoice);
        if(!$invoice->tenggat)
            $invoice->tenggat = '-';
        $templateFile = 'template_invoice.docx';
        $filename = $name . '.docx';
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path($templateFile));
        $templateProcessor->setValue('nomerInvoice', $invoice->nomor);
        $templateProcessor->setValue('perusahaan', $invoice->perusahaan->nama);
        $templateProcessor->setValue('contactPerson', "{$invoice->contact->nama} - {$invoice->contact->no_hp}");
        $templateProcessor->setValue('tanggal', $invoice->tanggal);
        $templateProcessor->setValue('tenggat', $invoice->tenggat);
        $templateProcessor->setValue('total', "Rp. " . number_format($invoice->total, '2', ',', '.'));
        $templateProcessor->setValue('keterangan', $invoice->keterangan);
        $templateProcessor->setValue('nominal', "Rp. " . number_format($invoice->total, '2', ',', '.'));
        $templateProcessor->setValue('terbilang', terbilang($invoice->total) . "rupiah");

        foreach ($invoice->item_set as $i) {
            $i['id'] = $x;
            $sub_total = "Rp. " . number_format($i['sub_total'], '2', ',', '.');
            $harga = "Rp. " . number_format($i['harga'], '2', ',', '.');
            $i['sub_total'] = $sub_total;
            $i['harga'] = $harga;
            array_push($item, $i);
            $x++;
        }
        $values = $item;
        $templateProcessor->cloneRowAndSetValues('id', $values);
        $templateProcessor->saveAs($filename);
        return response()->download($filename)->deleteFileAfterSend(true);
    }

    public function import(Request $request){
        $this->validate($request, [
            'excel' => 'required|file'
        ]);

        $nama_file = $request->excel->getClientOriginalName();

        $valid = false;
        $split = explode(".",$nama_file);
        $count = count($split);
        if($split && ($split[$count-1] === "xlsx" || $split[$count-1] === "xls" || $split[$count-1] === "csv")){
            $valid = true;
        }

        if($valid){
            $request->file('excel')->move(storage_path('app/public'), $nama_file);
            Excel::import(new InvoiceImport, storage_path('app/public/' . $nama_file));
            return redirect('invoice')->with('success', 'Invoice berhasil ditambahkan!');
        } else {
            return redirect('invoice')->with('failed', 'Invoice gagal ditambahkan!');
        }

    }

    public function cetakAll(Request $request)
    {
        $invoice = Invoice::select('id', 'perusahaans', 'cp', 'tanggal', 'tenggat', 'keterangan', 'invoice', 'item_set', 'total')->with(['contact:id,perusahaans,no_hp,nama', 'perusahaan:id,nama'])->withAnyTag(["{$request->tag}"]);
        $zip = new Madzipper();
        $file_zip = array();
        foreach ($invoice->get() as $invoice) {
            $item = array();
            $x = 1;
            $invoice->nomor = $invoice->invoice;
            $invoice->terbilang = terbilang($invoice->total) . " rupiah";
            $name = str_replace("/","_",$invoice->invoice);
            if(!$invoice->tenggat)
                $invoice->tenggat = '-';
            $templateFile = 'template_invoice.docx';
            $filename = $name . '.docx';
            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path($templateFile));
            $templateProcessor->setValue('nomerInvoice', $invoice->nomor);
            $templateProcessor->setValue('perusahaan', $invoice->perusahaan->nama);
            $templateProcessor->setValue('contactPerson', "{$invoice->contact->nama} - {$invoice->contact->no_hp}");
            $templateProcessor->setValue('tanggal', $invoice->tanggal);
            $templateProcessor->setValue('tenggat', $invoice->tenggat);
            $templateProcessor->setValue('total', "Rp. " . number_format($invoice->total, '2', ',', '.'));
            $templateProcessor->setValue('keterangan', $invoice->keterangan);
            $templateProcessor->setValue('nominal', "Rp. " . number_format($invoice->total, '2', ',', '.'));
            $templateProcessor->setValue('terbilang', terbilang($invoice->total) . "rupiah");

            foreach ($invoice->item_set as $i) {
                $i['id'] = $x;
                $sub_total = "Rp. " . number_format($i['sub_total'], '2', ',', '.');
                $harga = "Rp. " . number_format($i['harga'], '2', ',', '.');
                $i['sub_total'] = $sub_total;
                $i['harga'] = $harga;
                array_push($item, $i);
                $x++;
            }
            $values = $item;
            $templateProcessor->cloneRowAndSetValues('id', $values);
            $templateProcessor->saveAs(storage_path($filename));
            array_push($file_zip,storage_path($filename));
            // unlink(storage_path("{$filename}"));
        }
        // dd($file_zip);
        $zip->make(storage_path("{$request->tag}.zip"))->add($file_zip)->close();
        foreach ($file_zip as $file) {
            unlink($file);
        }
        return response()->download(storage_path("{$request->tag}.zip"))->deleteFileAfterSend(true);
    }
}
