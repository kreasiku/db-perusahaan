<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use \Conner\Tagging\Taggable;
    
    protected $table = "invoices";

    protected $casts = [
        'item_set' => 'array'
    ];

    protected $fillable = [
        'perusahaans',
        'cp',
        'user',
        'invoice',
        'keterangan',
        'tanggal',
        'tenggat',
        'item_set'
    ];

    
    function contact()
    {
        return $this->belongsTo('App\CpPerusahaan', 'cp', 'id');
    }

    function perusahaan()
    {
        return $this->belongsTo('App\Perusahaan', 'perusahaans', 'id');
    }

    function user()
    {
        return $this->belongsTo('App\User', 'user', 'id');
    }
}
