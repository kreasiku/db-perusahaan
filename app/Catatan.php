<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catatan extends Model
{
    protected $fillable = [
        'perusahaans',
        'catatans',
        'status',
        'follow_up',
        'user',
        'cp'
    ];

    protected $table = 'catatans';

    function status(){
        return $this->belongsTo('App\StatusCatatan', 'status', 'id');
    }

    function contact(){
        return $this->belongsTo('App\CpPerusahaan', 'cp', 'id');
    }

    function perusahaan(){
        return $this->belongsTo('App\Perusahaan', 'perusahaans', 'id');
    }

    function user()
    {
        return $this->belongsTo('App\User', 'user', 'id');
    }
}
