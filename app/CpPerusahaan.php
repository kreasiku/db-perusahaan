<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CpPerusahaan extends Model
{
    protected $fillable = [
        'perusahaans',
        'nama',
        'email',
        'no_hp',
        'tgl_lahir',
        'user'
    ];

    function catatan()
    {
        return $this->hasMany('App\Catatan', 'cp');
    }

    function invoice()
    {
        return $this->hasMany('App\Invoice', 'cp');
    }

    function status()
    {
        return $this->belongsTo('App\StatusCatatan', 'status', 'id');
    }

    function perusahaan(){
        return $this->belongsTo('App\Perusahaan', 'perusahaans', 'id');
    }
    
    function user()
    {
        return $this->belongsTo('App\User', 'user', 'id');
    }
}