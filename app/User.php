<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin','divisi', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    function perusahaan(){
        return $this->hasMany('App\Perusahaan', 'user');
    }

    function catatan()
    {
        return $this->hasMany('App\Catatan', 'user');
    }

    function contact()
    {
        return $this->hasMany('App\CpPerusahaan', 'user');
    }

    function invoice()
    {
        return $this->hasMany('App\Invoice', 'user');
    }
}
