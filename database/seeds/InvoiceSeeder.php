<?php

use App\Invoice;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Invoice::create([
            'perusahaans' => '1',
            'cp' => '1',
            'user' => '1',
            'keterangan' => 'coba-coba',
            'tanggal' => '2020-02-22',
            'tenggat' => '',
            'item_set' => '{"nama_item":"Roti","satuan":"kilo","jumlah":2,"harga":35000}'
        ]);
    }
}
