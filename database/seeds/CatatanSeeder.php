<?php

use Illuminate\Database\Seeder;
use App\Catatan;

class CatatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i=0; $i < 10000; $i++) {
            Catatan::create([
                'perusahaans' => '2',
                'catatans' => 'cobatestest',
                'status' => '1',
                'user' => '1',
                'cp' => '3',
            ]);
    }
}
}
