<?php

use Illuminate\Database\Seeder;
use App\User;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'User',
                'email' => 'user@ptkku.co.id',
                'is_admin' => 0,
                'password' => bcrypt('123'),
                'divisi' => 'Humas',
            ]
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
