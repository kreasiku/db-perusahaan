<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Createcatatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_catatans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status');
        });

        DB::table('status_catatans')->insert([
            ['status' => 'Sedang Diproses'],
            ['status' => 'Gagal'],
            ['status' => 'Selesai']
        ]);

        Schema::create('catatans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('perusahaans')->nullable();
            $table->text('catatans');
            $table->unsignedBigInteger('status');
            $table->unsignedBigInteger('user')->nullable();
            $table->unsignedBigInteger('cp')->nullable();
            $table->date('follow_up')->nullable();
            $table->timestamps();

            $table->foreign('user')->references('id')->on('users')->onDelete('set null');
            $table->foreign('perusahaans')->references('id')->on('perusahaans')->onDelete('set null');
            $table->foreign('status')->references('id')->on('status_catatans')->onDelete('cascade');
            $table->foreign('cp')->references('id')->on('cp_perusahaans')->onDelete('set null');
        });

        DB::table('catatans')->insert([
            'perusahaans' => 1,
            'catatans' => 'Follow up Pembayaran Registrasi',
            'status' => 1,
            'user' => 2,
            'cp' => 1,
            'follow_up' => Carbon::now(),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catatans');
        Schema::dropIfExists('status_catatans');
    }
}
