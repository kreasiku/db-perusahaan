<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CreateEventPerusahaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_perusahaans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('nama');
            $table->unsignedBigInteger('perusahaans')->nullable();
            $table->date('tanggal');
            $table->text('keterangan');
            $table->unsignedBigInteger('user')->nullable();
            $table->timestamps();

            $table->foreign('perusahaans')->references('id')->on('perusahaans')->onDelete('set null');
            $table->foreign('user')->references('id')->on('users')->onDelete('set null');
        });

        DB::table('event_perusahaans')->insert([
            'nama' => 'Buka bersama',
            'perusahaans' => 1,
            'tanggal' => Carbon::now(),
            'keterangan' => 'Buka bersama karyawan',
            'user' => 2,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_perusahaans');
    }
}
