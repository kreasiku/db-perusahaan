<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->boolean('is_admin')->default(0);
            $table->string('password');
            $table->string('avatar')->default("vendor/adminlte/dist/img/user4-128x128.jpg");
            $table->string('divisi');
            $table->timestamps();
        });

        DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'admin@ptkku.co.id',
            'is_admin' => 1,
            'password' => bcrypt('admin123'),
            'divisi' => 'IT',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);

        DB::table('users')->insert([
            'name' => 'User',
            'email' => 'user@ptkku.co.id',
            'password' => bcrypt('12345'),
            'divisi' => 'Humas',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }

}
