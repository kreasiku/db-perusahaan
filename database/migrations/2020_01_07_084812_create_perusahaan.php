<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CreatePerusahaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        $date = Carbon::now();

        Schema::create('perusahaans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama')->unique();
            $table->string('bidang');
            $table->text('deskripsi');
            $table->date('tanggal_berdiri')->nullable();
            $table->unsignedBigInteger('user')->nullable();
            $table->timestamps();
            
            $table->foreign('user')->references('id')->on('users')->onDelete('set null');
        });

        DB::table('perusahaans')->insert([
            'nama' => 'Hiden Intelligence',
            'bidang' => 'Teknologi',
            'deskripsi' => 'Perusahaan pemroduksi HumaGear',
            'tanggal_berdiri' => '2012-12-20',
            'user' => 2,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        
        Schema::create('cp_perusahaans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('perusahaans')->nullable();
            $table->string('nama')->unique();
            $table->string('email')->nullable()->unique();
            $table->string('no_hp');
            $table->date('tgl_lahir')->nullable();
            $table->unsignedBigInteger('user')->nullable();
            $table->timestamps();

            $table->foreign('user')->references('id')->on('users')->onDelete('set null');
            $table->foreign('perusahaans')->references('id')->on('perusahaans')->onDelete('set null');
            
        });

        DB::table('cp_perusahaans')->insert([
            'perusahaans' => 1,
            'nama' => 'Prada',
            'email' => 'prada@hiden.org',
            'no_hp' => '0212281305',
            'tgl_lahir' => '1999-09-19',
            'user' => 2
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perusahaans');
        Schema::dropIfExists('cp_perusahaans');
    }
}
