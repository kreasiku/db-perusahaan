<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('keterangan', 255);
            $table->unsignedBigInteger('perusahaans')->nullable();
            $table->unsignedBigInteger('cp')->nullable();
            $table->date('tanggal')->default(Carbon::now()->toDateString());
            $table->date('tenggat')->nullable();
            $table->longText('item_set');
            $table->string('invoice')->nullable();
            $table->unsignedBigInteger('total')->default(0);
            $table->unsignedBigInteger('user')->nullable();
            $table->timestamps();

            $table->foreign('user')->references('id')->on('users')->onDelete('set null');
            $table->foreign('perusahaans')->references('id')->on('perusahaans')->onDelete('set null');
            $table->foreign('cp')->references('id')->on('cp_perusahaans')->onDelete('set null');
        });

        DB::table('invoices')->insert([
            'keterangan' => 'astaghfirullah',
            'perusahaans' => 1,
            'cp' => 1,
            'user' => 1,
            'total' => 12200000,
            'item_set' => '[{"nama":"anu","satuan":"biji","jumlah":"12","harga":"1000000","sub_total":"12000000"},
                            {"nama":"benu","satuan":"ekor","jumlah":"2","harga":"100000","sub_total":"200000"}]',
            'invoice' => '1/SINV/02/2020',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
