@extends('adminlte::page')

@section('title', 'Manajemen Perusahaan - Sistem Administrasi Internal PT KKU')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>Manajemen Perusahaan</h1>
@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">

<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

    th {
        text-align: center !important;
    }

    td {
        vertical-align: middle !important;
    }

    .word-wrap {
        word-break: break-all;
        /* word-wrap: break-word; */
        /* overflow-wrap: break-word; */
    }

    .no-wrap {
        white-space: nowrap;
    }

    .fixed {
        table-layout: fixed;
    }

    .card-header .navbar {
        height: 40px;
        flex-wrap: nowrap;
    }
</style>
@stop

@section('content')

<!-- Message -->
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@elseif ($message = Session::get('failed'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@endif
<!-- Message -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="navbar" id="card-header">
                    <h3 class="card-title align-middle">Daftar Perusahaan</h3>
                    <div class="navbar d-flex card-tools">
                        <a class="btn btn-sm btn-outline-secondary bd-highlight" href="{{ route('perusahaan.create') }}"><i class="fas fa-plus"></i> Tambah</a>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
                <table class="table table-hover dataTables text-center w-100" id="perusahaan-table">
                    <thead class="p-0">
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Bidang</th>
                            <th class="text-center">Deskripsi</th>
                            <th>Tanggal Berdiri</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="p-0">
                        <!-- Modal Edit -->
                        <div class="modal fade edit-perusahaan">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h4 class="modal-title">Ubah Perusahaan</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form id="form-me" method="POST" action="#">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <input hidden id="id-me" type="text" class="form-control" name="pid" required autocomplete="pid" autofocus>

                                            <div class="form-group row">
                                                <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Perusahaan') }}</label>

                                                <div class="col-md-6">
                                                    <input id="nama-me" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" required autocomplete="nama" autofocus>

                                                    @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="bidang" class="col-md-4 col-form-label text-md-right">{{ __('Bidang Perusahaan') }}</label>

                                                <div class="col-md-6">
                                                    <input id="bidang-me" type="text" class="form-control @error('bidang') is-invalid @enderror" name="bidang" required autocomplete="bidang">

                                                    @error('bidang')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="deskripsi" class="col-md-4 col-form-label text-md-right">{{ __('Deskripsi Perusahaan') }}</label>

                                                <div class="col-md-6">
                                                    <input id="deskripsi-me" type="text" class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi" required autocomplete="deskripsi">

                                                    @error('deskripxsi')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="tanggal_berdiri" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Berdiri') }}</label>

                                                <div class="col-md-6">
                                                    <input readonly id="tanggal_berdiri-me" type="text" class="form-control @error('tanggal_berdiri') is-invalid @enderror" name="tanggal_berdiri" autocomplete="tanggal_berdiri">

                                                    @error('tanggal_berdiri')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Model Footer -->
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- End of Modal Edit -->

                        <!-- Modal Hapus -->
                        <div class="modal fade delete-perusahaan">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h4 class="modal-title">Apakah Anda yakin ingin menghapus Perusahaan berikut?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Perusahaan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="nama-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Bidang') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="bidang-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Jumlah Catatan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="count-md"></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="modal-footer">
                                            <form id="form-md" action="">
                                                <input id="id-md" name="id" hidden>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <!-- End of Modal Hapus -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@stop

@section('js')
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="/js/dataTables-num-html.js"></script>

<script>
    var pwd = window.location.pathname;
    pwd = pwd.substring(0, pwd.lastIndexOf('/'));
    if (pwd.length > 1) {
        pwd += "/";
    } else {
        pwd += "perusahaan/";
    }

    function buttonClick(id){
        console.log(id);
        $("#form-me").attr("action", pwd + "update/" + id);
        $("#form-md").attr("action", pwd + "delete/" + id);
        $.ajax({
            url: '{{ route("perusahaan.getJson") }}',
            dataType: 'json',
            data: {
                id: id
            },
            // });
            success: function(data) {
                if (data) {
                    var p = data;
                    console.log(p);
                    $("#id-md").val(p['id'])
                    $('#id-me').val(p['id']);

                    $("#nama-me").val(p['nama']);
                    $("#nama-md").html(p['nama']);

                    $("#bidang-me").val(p['bidang']);
                    $("#bidang-md").html(p['bidang']);

                    $("#count-md").html(p['count']);
                    $("#deskripsi-me").val(p['deskripsi']);
                    $("#tanggal_berdiri-me").val(p['tanggal_berdiri']);
                }
            }
        });
    }
    $(document).ready(function() {
        $("#perusahaan-table").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: pwd + 'json',
            columns: [
                { data: 'created_at',
                render:
                    function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'nama', name: 'nama' },
                { data: 'bidang', name: 'bidang'},
                { data: 'deskripsi', name: 'deskripsi', className: 'text-left'},
                { data: 'tanggal_berdiri', name:'tanggal_berdiri'},
                { data: 'aksi', name:'aksi', className: 'no-wrap'},

            ]
        });

        $("#status-me").css('width', '100%');
        $("#status-me").select2();
        $("#follow_up-me").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#tanggal_berdiri-me').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $("button.close").click(function () {
            $("#form-me").attr("action", "#");
            $("#form-md").attr("action", "#");
        });
    });
</script>
@stop
