@extends('adminlte::page')

@section('title', 'Manajemen Perusahaan - Sistem Administrasi Internal PT KKU')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>{{$perusahaan['nama']}}</h1>
@stop

@section('sidebar_collapse', 'true')

@section('css')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

    th {
        text-align: center !important;
    }

    td {
        vertical-align: middle !important;
    }

    .word-wrap {
        word-break: break-all;
        /* word-wrap: break-word; */
        /* overflow-wrap: break-word; */
    }

    .no-wrap {
        white-space: nowrap;
    }

    .fixed {
        table-layout: fixed;
    }
</style>
@stop

@section('content')
<div class="row">
    <section class="col-lg-7">
        <!-- Staff -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Daftar Staff</h3>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-hover dataTables no-wrap text-center" id="contact-show">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Staff</th>
                            <th>Tanggal Lahir</th>
                            <th>Nomor HP</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- Catatan -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Daftar Catatan</h3>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-hover dataTables text-center" id="catatan-show">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-center">Catatan</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- End of Catatan -->
    </section>
    <section class="col-lg-5">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Perusahaan</h3>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="tanggal_berdiri" class="col-md-4 col-form-label text-md-right">{{ __('Didirikan pada') }}</label>

                    <div class="col-md-6">
                        <p class="col-form-label">{{$perusahaan['tanggal_berdiri']}}</p>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="deskripsi" class="col-md-4 col-form-label text-md-right">{{ __('Deskripsi') }}</label>

                    <div class="col-md-6">
                        <p class="col-form-label">{{$perusahaan['deskripsi']}}</p>
                    </div>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </section>
</div>

@stop
@section('js')
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        var pwd = window.location.pathname;
        // console.log(pwd);
        pwd = pwd.substring(0);
        // console.log(pwd.length);
        if (pwd.length > 1) {
            pwd += "/";
        } else {
            pwd += "perusahaan/";
        }
        // console.log(pwd);
        $('#contact-show').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [
                [5, 10, 25, 50, 100],
                [5, 10, 25, 50, 100]
            ],
            ajax: pwd + "contact_json",
            columns: [{
                    data: 'created_at',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'nama',
                    name: 'nama'
                },
                {
                    data: 'tgl_lahir',
                    name: 'tgl_lahir'
                },
                {
                    data: 'no_hp',
                    name: 'no_hp'
                },
            ]
        });

        $('#catatan-show').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [
                [5, 10, 25, 50, 100],
                [5, 10, 25, 50, 100]
            ],
            ajax: pwd + "catatan_json",
            columns: [{
                    data: 'created_at',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'catatans',
                    name: 'catatans',
                    className: 'text-left text-wrap'
                },
                {
                    data: 'status',
                    name: 'status'
                },
            ]
        });
    });
</script>
@stop