@extends('adminlte::page')

@section('title', 'Event Perusahaan - Sistem Administrasi Internal PT KKU')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>Event Perusahaan</h1>
@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

    th {
        text-align: center !important;
    }

    td {
        vertical-align: middle !important;
    }

    .word-wrap {
        word-break: break-all;
        /* word-wrap: break-word; */
        /* overflow-wrap: break-word; */
    }

    .no-wrap {
        white-space: nowrap;
    }

    .fixed {
        table-layout: fixed;
    }

    .card-header .navbar {
        height: 40px;
        flex-wrap: nowrap;
    }
</style>
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"> --}}
@stop

@section('content')

<!-- Message -->
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@elseif ($message = Session::get('failed'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@endif
<!-- Message -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="navbar" id="card-header">
                    <h3 class="card-title align-middle">Daftar Event</h3>
                    <div class="navbar d-flex card-tools">
                        <a class="btn btn-sm btn-outline-secondary bd-highlight" href="{{ route('event.create') }}"><i class="fas fa-plus"></i> Tambah</a>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
                <table class="table table-hover text-center dataTables" id="event-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Event</th>
                            <th>Perusahaan</th>
                            <th>Tanggal</th>
                            <th class="text-center">Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Modal View -->
                        <div class="modal fade view-event">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-success">
                                        <h4 class="modal-title">Detail Catatan</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Nama Event') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="nama-mv"></div>

                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Perusahaan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="perusahaans-mv"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Tanggal') }}</div>

                                                <div class="col text-md-left col-form-label col-md-4" id="tanggal-mv"></div>

                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Keterangan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="keterangan-mv"></div>
                                                <div class="w-100"></div>

                                            </div>
                                        </div>
                                        <br>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default float-right" data-dismiss="modal">Tutup</button>
                                            {{-- <button type="submit" class="btn btn-primary">Save changes</button> --}}
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <!-- /.modal -->

                        <!-- Modal Edit -->
                        <div class="modal fade edit-event">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h4 class="modal-title">Ubah Event</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form id="form-me" method="POST" action="#">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <input hidden id="id-me" type="text" class="form-control" name="eid" required autocomplete="eid" autofocus>

                                            <div class="form-group row">
                                                <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Event') }}</label>

                                                <div class="col-md-6">
                                                    <input id="nama-me" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>

                                                    @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="perusahaans" class="col-md-4 col-form-label text-md-right">{{ __('Perusahaan') }}</label>

                                                <div class="col-md-6 word-wrap">
                                                    <input readonly id="perusahaans-me" type="text" class="js-example-basic-single form-control @error('perusahaans') is-invalid @enderror" name="perusahaans" required autocomplete="perusahaans">

                                                    @error('perusahaans')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="tanggal" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Event') }}</label>

                                                <div class="col-md-6">
                                                    <input id="tanggal-me" type="text" class="form-control @error('tanggal') is-invalid @enderror" name="tanggal" value="{{ old('tanggal') }}" required autocomplete="tanggal">

                                                    @error('tanggal')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="keterangan" class="col-md-4 col-form-label text-md-right">{{ __('Keterangan') }}</label>

                                                <div class="col-md-6">
                                                    <textarea id="keterangan-me" rows="5" class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" required autocomplete="keterangan" autofocus></textarea>

                                                    @error('keterangan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row" hidden>
                                                <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('User') }}</label>

                                                <div class="col-md-6">
                                                    <input id="user-me" type="text" value="{{ Auth::user()->id }}" class="form-control @error('user') is-invalid @enderror" name="user" required autocomplete="user">

                                                    @error('user')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal edit end -->

                        <!-- Modal Delete -->
                        <div class="modal fade delete-event">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h4 class="modal-title">Apakah Anda yakin ingin menghapus Event berikut?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Nama Event') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="nama-md"></div>

                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Perusahaan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="perusahaans-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Tanggal') }}</div>

                                                <div class="col text-md-left col-form-label col-md-4" id="tanggal-md"></div>

                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Keterangan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="keterangan-md"></div>
                                                <div class="w-100"></div>

                                            </div>
                                        </div>
                                        <br>
                                        <div class="modal-footer">
                                            <form id="form-md" action="#">
                                                <input id="id-md" name="id" hidden>
                                                <button class="btn btn-danger" id="btn-md">Hapus</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <!-- /.modal -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@stop

@section('js')
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script>
    var total = '{{ \App\CpPerusahaan::count() }}';
    var pwd = window.location.pathname;
    console.log(pwd);

    pwd = pwd.substring(0, pwd.lastIndexOf('/'));
    console.log(pwd);
    if (pwd.length > 1) {
        pwd += "/";
    } else {
        pwd += "event/";
    }

    function buttonClick(id) {
        $('#id-me').val("");
        $('#id-md').val("");

        $("#nama-mv").html("");
        $("#nama-me").val("");
        $("#nama-md").html("");

        $("#perusahaans-mv").html("");
        $("#perusahaans-me").val("");
        $("#perusahaans-md").html("");

        $("#tanggal-me").val("");
        $("#tanggal-mv").html("");
        $("#tanggal-md").html("");

        $("#keterangan-me").val("");
        $("#keterangan-mv").html("");
        $("#keterangan-md").html("");

        $.ajax({
            url: '{{ route("event.getJson") }}',
            dataType: 'json',
            data: {
                id: id
            },
            success: function(data) {
                if (data) {
                    $("#form-me").attr("action", pwd + "update/" + id);
                    $("#form-md").attr("action", pwd + "delete/" + id);
                    console.log(data);
                    var e = data;
                    $('#id-me').val(e['id']);
                    $('#id-md').val(e['id']);

                    $("#nama-mv").html(e['nama']);
                    $("#nama-me").val(e['nama']);
                    $("#nama-md").html(e['nama']);

                    if (e['perusahaan'] != null) {
                        $("#perusahaans-mv").html(e['perusahaan']['nama']);
                        $("#perusahaans-me").val(e['perusahaan']['nama']);
                        $("#perusahaans-md").html(e['perusahaan']['nama']);
                    } else {
                        $("#perusahaans-mv").html(e['perusahaans']);
                        $("#perusahaans-me").val(e['perusahaans']);
                        $("#perusahaans-md").html(e['perusahaans']);
                    }

                    $("#tanggal-me").val(e['tanggal']);
                    $("#tanggal-mv").html(e['tanggal']);
                    $("#tanggal-md").html(e['tanggal']);

                    $("#keterangan-me").val(e['keterangan']);
                    $("#keterangan-mv").html(e['keterangan']);
                    $("#keterangan-md").html(e['keterangan']);
                }
            }
        });
    }

    $(document).ready(function() {
        $("#event-table").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: pwd + 'json',
            columns: [
                { data: 'created_at',
                render:
                    function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'nama', name: 'nama' },
                { data: 'perusahaans', name: 'perusahaans'},
                { data: 'tanggal', name: 'tanggal'},
                { data: 'keterangan', name:'keterangan',  className: 'text-left'},
                { data: 'aksi', name:'aksi', className: 'no-wrap'},
            ]
        });

        console.log(pwd);

        $('#perusahaans-me').css('width', '100%');

        $("#tanggal-me").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $("button.close").click(function () {
            $("#form-me").attr("action", "#");
            $("#form-md").attr("action", "#");
        });
    });
</script>
@stop
