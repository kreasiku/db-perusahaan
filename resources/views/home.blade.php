@extends('adminlte::page')

@section('title', 'Dashboard - Sistem Administrasi Internal PT KKU')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>Dashboard</h1>
@stop

@section('css')
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="css/fullcalendar.min.css">
<link rel="stylesheet" href="css/fullcalendar.print.min.css" media="print">
<link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

	img.cropped {
		width: 100px !important;
		height: 100px;
	}

    .fc-today {
        background: #D4D4D4 !important;
    }
</style>
@stop

@section('content')
<section class="content">
    <div class="container-fluid">

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-8 connectedSortable">

                <div class="card">
                    <div class="card-header border-0">
                        <h3 class="card-title">Follow up</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped table-valign-middle text-center dataTables" id="today-note">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center">Catatan</th>
                                    <th>Contact</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Calendar -->
                <div class="card">
                    <div class="card-header border-0">

                        <h3 class="card-title">
                            <i class="far fa-calendar-alt"></i>
                            Calendar
                        </h3>
                        <!-- tools card -->
                        <div class="card-tools">
                            <!-- button with a dropdown -->
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <i class="fas fa-bars"></i></button>
                                <div class="dropdown-menu float-right" role="menu">
                                    <a href="#" class="dropdown-item">Add new event</a>
                                    <a href="#" class="dropdown-item">Clear events</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="#" class="dropdown-item">View calendar</a>
                                </div>
                            </div>
                            <button type="button" class="btn btn-default btn-sm" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-default btn-sm" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body pt-0">
                        <!--The calendar -->
                        <div id="calendar" style="width: 100%"></div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </section>
            <!-- /.Left col -->

            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-4 connectedSortable">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ \App\Catatan::where('user',Auth::user()->id)->count() }}</h3>

                        <p>Catatan masuk</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-list"></i>
                    </div>
                    <a href="catatan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>

                <div class="small-box bg-secondary">
                    <div class="inner">
                        <h3>{{ \App\Perusahaan::where('user',Auth::user()->id)->count() }}</h3>

                        <p>Perusahaan</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-briefcase"></i>
                    </div>
                    <a href="perusahaan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </section>
            <!-- right col -->
        </div>
        <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
</section>
@stop

@section('js')
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/jquery-ui.custom.min.js"></script>
<script src="js/fullcalendar.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/dataTables-num-html.js"></script>
<script>
    var stat = "#status";
    var total = '{{ \App\Catatan::count() }}';

    $(document).ready(function() {
        var SITEURL = "{{url('/')}}";

        $('#today-note').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": false,
            "lengthMenu": [5, 10, 25, 50, 100],
            ajax: 'home/json',
            columns: [{
                    data: 'id',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'catatans',
                    name: 'catatans',
                    className:'text-left text-wrap'
                },
                {
                    data: 'contacts',
                    name: 'contacts',
                    className: 'text-nowrap'
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });

        for (let x = 1; x <= total; x++) {
            $(stat.concat("-" + x)).css('width', '100%');
            $(stat.concat("-" + x)).select2();
            $("#follow_up-" + x).datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
        };

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next,today,month',
                center: '',
                right: 'title'
            },
            eventTextColor: '#FFFFFF',
            height: "auto",
            navLinks: true,
            defaultDate: Date(),
            editable: false,
            showNonCurrentDates: false,
            events: {
                url: "{{ route('calendar') }}",
            },
            displayEventTime: true,
            eventRender: function(event, element, view) {
                console.log(event);
            }
        });
    });
</script>
@stop