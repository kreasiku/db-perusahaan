@extends('adminlte::page')

@section('title', 'Invoice - Sistem Administrasi Internal PT KKU')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>Invoice</h1>
@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/css/bootstrap-tagsinput.css">
<style>
    th {
        text-align: center !important;
    }

    td {
        vertical-align: middle !important;
    }

    .word-wrap {
        word-break: break-all;
        /* word-wrap: break-word; */
        /* overflow-wrap: break-word; */
    }

    .no-wrap {
        white-space: nowrap;
    }

    .fixed {
        table-layout: fixed;
    }

    .card-header .navbar {
        height: 40px;
        flex-wrap: nowrap;
    }

    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

    .tagging {
        margin-right: 2px;
    }
</style>
@stop

@section('content')
<!-- Message -->
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@elseif ($message = Session::get('failed'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@endif
<!-- Message -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="navbar" id="card-header">
                    <h3 class="card-title align-middle">Daftar Invoice</h3>
                    <div class="navbar d-flex card-tools">
                        <form action="{{ route('invoice.printAll') }}">
                            <button class="btn btn-sm btn-outline-primary" name="tag" type="submit" id="print-all" hidden><i class="fas fa-download"></i> Export All (docx)</button>
                        </form>
                        <form action="{{ route('invoice.template') }}">
                            <button id="template" class="btn btn-sm btn-outline-success mr-1" type="submit"><i class="fas fa-download"></i> Download Template</button>
                        </form>
                        <form action="{{ route('invoice.import') }}" id="import-form" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="excel" id="excel" hidden>
                            <button type="button" id="import" class="btn btn-sm btn-outline-primary mr-1"><i class="fas fa-upload"></i> Import Excel</button>
                        </form>
                        <a id="tambah" class="btn btn-sm btn-outline-secondary bd-highlight" href="{{ route('invoice.create') }}"><i class="fas fa-plus"></i> Tambah</a>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
                <table class="table table-hover dataTables text-center" id="invoice-table">
                    <thead class="p-0">
                        <tr>
                            <th>Invoice</th>
                            <th>Tanggal</th>
                            <th>Jatuh Tempo</th>
                            <th class="text-center">Keterangan</th>
                            <th>Contact<br>Perusahaan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="p-0">

                    </tbody>
                </table>

                <!-- Modal View -->
                <div class="modal fade view-invoice">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-success">
                                <h4 class="modal-title">Detail Invoice</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="text-md-left col-form-label col-md-2 font-weight-bold">{{ __('Perusahaan') }}</div>
                                        <div class="text-md-left col-form-label col-md-4" id="perusahaan-mv"></div>

                                        <div class="text-md-left col-form-label col-md-2 font-weight-bold">{{ __('Nomor Invoice') }}</div>
                                        <div class="text-md-left col-form-label col-md-4" id="nomor-mv"></div>

                                        <div class="text-md-left col-form-label col-md-2 font-weight-bold">{{ __('Contact Person') }}</div>
                                        <div class="text-md-left col-form-label col-md-4" id="cp-mv"></div>

                                        <div class="text-md-left col-form-label col-md-2 font-weight-bold">{{ __('Jatuh Tempo') }}</div>
                                        <div class="text-md-left col-form-label col-md-4" id="tenggat-mv"></div>

                                        <div class="text-md-left col-form-label col-md-2 font-weight-bold">{{ __('Tanggal') }}</div>
                                        <div class="text-md-left col-form-label col-md-4" id="tanggal-mv"></div>

                                    </div>
                                    <br>
                                    <div class="row table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <th>Nama Item</th>
                                                <th>Satuan</th>
                                                <th>Jumlah</th>
                                                <th>Harga</th>
                                                <th>Sub Total</th>
                                            </thead>
                                            <tbody id="detail-invoice">

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col col-md-8">
                                            <div class="row">
                                                <div class="col text-md-left col-md-3 font-weight-bold">{{ __('Keterangan') }}</div>
                                                <div class="col text-md-left col-md-9" id="keterangan-mv"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col text-md-left col-md-3 font-weight-bold">{{ __('Nominal') }}</div>
                                                <div class="col text-md-left col-md-9" id="nominal-mv"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col text-md-left col-md-3 font-weight-bold">{{ __('Terbilang') }}</div>
                                                <div class="col text-md-left col-md-9" id="terbilang-mv"></div>
                                            </div>
                                        </div>
                                        <div class="col col-md-4">

                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="modal-footer">
                                    <form id="cetak" method="POST" action="#">
                                        @csrf
                                    <button type="submit" class="btn btn-outline-success float-right"><i class="fas fa-print"></i> Cetak</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                </div>
                <!-- /.modal -->

                <!-- Modal Edit -->
                <div class="modal fade edit-invoice">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-info">
                                <h4 class="modal-title">Ubah Invoice</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <form id="form-me" method="POST" action="#">
                                @csrf
                                @method('PUT')
                                <input id="id-me" name="id" hidden>
                                <div class="modal-body">
                                    <ul class="nav nav-tabs card-header-tabs" id="invoice-list" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#form1" role="tab" aria-controls="form1">Invoice</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#form2" role="tab" aria-controls="form2">Item</a>
                                        </li>
                                    </ul>
                                    <br>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="form1" role="tabpanel">
                                            <div class="form-group row">
                                                <label for="cp" class="col-md-4 col-form-label text-md-right">{{ __('Contact Person') }}</label>
                                                <div class="col-md-6">
                                                    <input id="cp-me" readonly type="text" class="form-control @error('form-cp') is-invalid @enderror" name="form-cp" required autocomplete="form-cp" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="invoice" class="col-md-4 col-form-label text-md-right">{{ __('Nomor Invoice') }}</label>

                                                <div class="col-md-6">
                                                    <input id="invoice" type="text" class="form-control @error('invoice') is-invalid @enderror" name="invoice" autocomplete="invoice">

                                                    @error('invoice')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="keterangan" class="col-md-4 col-form-label text-md-right">{{ __('Keterangan') }}</label>
                                                <div class="col-md-6">
                                                    <textarea id="keterangan" rows="5" class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" required autocomplete="keterangan" autofocus></textarea>
                                                    @error('keterangan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tanggal-invoice" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Invoice') }}</label>
                                                <div class="col-md-6">
                                                    <input id="tanggal-invoice" type="text" value="" class="form-control @error('tanggal-invoice') is-invalid @enderror select-date" name="tanggal" autocomplete="tanggal-invoice">
                                                    @error('tanggal-invoice')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="tenggat-invoice" class="col-md-4 col-form-label text-md-right">{{ __('Jatuh Tempo') }}</label>
                                                <div class="col-md-6">
                                                    <input id="tenggat-invoice" type="text" value="" class="form-control @error('tenggat-invoice') is-invalid @enderror select-date" name="tenggat" autocomplete="tenggat-invoice">
                                                    @error('tenggat-invoice')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row" hidden>
                                                <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('User') }}</label>
                                                <div class="col-md-6">
                                                    <input id="user" type="text" value="{{ Auth::user()->id }}" class="form-control @error('user') is-invalid @enderror" name="user" required autocomplete="user">
                                                    @error('user')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="tags" class="col-md-4 col-form-label text-md-right">{{ __('Tags') }}</label>

                                                <div class="col-md-6" id="input-tags">

                                                    <input id="tags" type="text" data-role="tagsinput" class="form-control @error('tags') is-invalid @enderror" name="tags" autocomplete="tags">

                                                    @error('tags')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="form2" role="tabpanel">
                                            <div class="form-group row">
                                                <table class="table text-center">
                                                    <thead>
                                                        <tr>
                                                            <th>Nama</th>
                                                            <th>Satuan</th>
                                                            <th>Jumlah</th>
                                                            <th>Harga</th>
                                                            <td><button type="button" name="add" id="add" class="btn btn-success"><i class="fas fa-plus"></i></button></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="dynamicTable">

                                                    </tbody>


                                                    @error('item_set')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal edit end -->

                <!-- Modal Delete -->
                <div class="modal fade delete-invoice">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-danger">
                                <h4 class="modal-title">Apakah anda yakin menghapus invoice berikut?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col text-md-left col-form-label col-md-4">Nomor Invoice</div>
                                        <div class="col text-md-left col-form-label col-md-4" id="nomor-md"></div>
                                        <div class="w-100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <form id="form-md" action="">
                                    <input id="id-md" name="id" hidden>
                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal -->
            </div>
            <!-- /.card-body -->

        </div>
        <!-- /.card -->
    </div>
</div>
@stop

@section('js')
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="/js/dataTables-num-html.js"></script>
<script src="/js/bootstrap-tagsinput.js"></script>
<script>
    var pwd = window.location.pathname;
    pwd = pwd.substring(0, pwd.lastIndexOf('/'));

    if (pwd.length > 1) {
        pwd += "/";
    } else {
        pwd += "invoice/";
    }

    $('button#import').click(function () {
        $('#excel').click();
    });
    
    $('#excel').change(function () {
        console.log(this.files);
        if (this.files && $(this).val()) {
            $('button#import').attr("disabled", true);
            console.log($('#import-form').attr("action"));
            document.getElementById("import-form").submit();
        }
    });


    function removeLastComma(str) {
        var n = str.lastIndexOf(",");
        var a = str.substring(0,n);
        return a;
    }

    $('#invoice-list a').on('click', function(e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $(document).on('click', '.remove-tr', function() {
        $(this).parents('tr').remove();
    });

    $(document).on('click', '.tagging', function() {
        $('h1').html("Invoice");
        $('h1').html("Invoice " + "<span class='badge badge-warning'>" + this.value + "</span>");
        console.log(this.value);
        $('button#import').attr("hidden", true);
        $('button#template').attr("hidden", true);
        $('a#tambah').attr("hidden", true);
        $('button#print-all').attr("hidden", false);
        $('button#print-all').attr("value", this.value);
        $('#invoice-table').DataTable().destroy();
        $('#invoice-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: false,
            ajax: {
                url: pwd + 'tag-filter',
                data: {
                    tag: this.value
                }
            },
            columnDefs: [{
                targets: [0, 2, 3, 4, 5],
                className: 'text-nowrap'
            }],
            columns: [{
                    data: 'invoice',
                    name: 'invoice'
                },
                {
                    data: 'tanggal',
                    name: 'tanggal'
                },
                {
                    data: 'tenggat',
                    name: 'tenggat',
                    render: function(data, type, row, meta) {
                        if (!data) {
                            return '-'
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'keterangan',
                    name: 'keterangan',
                    className: 'text-left text-wrap'
                },
                {
                    data: 'contact',
                    defaultContent: '-',
                    type: 'html'
                },
                {
                    data: 'aksi',
                    name: 'aksi'
                },
            ]
        });
    });

    function buttonClick(id) {
        $('#nomor-md').html("");
        $('#detail-invoice').html("");
        $('#dynamicTable').html("");
        console.log(id);
        $.ajax({
            url: '{{ route("invoice.getData") }}',
            dataType: 'json',
            data: {
                id: id
            },
            success: function(data) {
                if (data) {
                    var invoice = data;
                    // console.log(data);
                    let total = 0;
                    var date = new Date(invoice['tanggal']);
                    var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
                    // console.log(date.getDate())
                    // Delete
                    $('#form-md').attr('action', pwd + 'delete/' + id);
                    $('#id-md').val(invoice['id']);
                    $('#nomor-md').html(invoice['invoice']);

                    //Edit
                    var i = invoice['item_set']['length'];
                    $("#add").click(function() {
                        i++;
                        $("#dynamicTable").append('<tr><td><input type="text" name="item_set[' + i + '][nama]" placeholder="Nama item" class="form-control @error("item_set") is-invalid @enderror" required /></td><td><input type="text" name="item_set[' + i + '][satuan]" placeholder="Satuan" class="form-control @error("item_set") is-invalid @enderror" required /></td><td><input type="number" name="item_set[' + i + '][jumlah]" placeholder="Jumlah" class="form-control @error("item_set") is-invalid @enderror" required /></td><td><input type="number" name="item_set[' + i + '][harga]" placeholder="Harga satuan" class="form-control @error("item_set") is-invalid @enderror" required /></td><td><button type="button" class="btn btn-danger remove-tr"><i class="fas fa-minus"></i></button></td></tr>');
                    });
                    $('#form-me').attr('action', pwd + 'update/' + id);
                    $('#cetak').attr('action', pwd + 'cetak/' + id).attr('target','_blank');

                    $('#id-me').val(invoice['id']);
                    if (invoice['contact'] != null) {
                        $("#cp-me").val(invoice['contact']['nama']);
                    } else {
                        $("#cp-me").val("");
                    }

                    if (invoice['contact'] != null) {
                        $("#cp-me").val(function() {
                            return this.value + ' - ' + invoice['perusahaan']['nama'];
                        });
                    } else {
                        $("#cp-me").val(function() {
                            return this.value + ' - ' + invoice['perusahaans'];
                        });
                    }
                    console.log(invoice);
                    $('#keterangan').val(invoice['keterangan'])
                    $('#tanggal-invoice').val(invoice['tanggal']);
                    $('#tenggat-invoice').val(invoice['tenggat']);
                    $('#invoice').val(invoice['invoice']);
                    $('#tags').tagsinput('removeAll');
                    var existTag = '';
                    var tagged = invoice['tagged'];
                    tagged.forEach(function (i) {
                        existTag += i['tag_slug'] + ',';
                    });
                    $('#tags').tagsinput('add', removeLastComma(existTag));
                    var itemSet = invoice['item_set'];
                    var index = 0;
                    itemSet.forEach(function(i) {
                        // append tabel item
                        index++;
                        var tableDetail = '<tr><td><input type="text" name="item_set[' + index + '][nama]" value="' + i['nama'] + '" placeholder="Nama item" class="form-control @error("item_set") is-invalid @enderror" required /></td><td><input type="text" name="item_set[' + index + '][satuan]" value="' + i['satuan'] + '" placeholder="Satuan" class="form-control @error("item_set") is-invalid @enderror" required /></td><td><input type="number" name="item_set[' + index + '][jumlah]" value="' + i['jumlah'] + '" placeholder="Jumlah" class="form-control @error("item_set") is-invalid @enderror" required /></td><td><input type="number" name="item_set[' + index + '][harga]" value="' + i['harga'] + '" placeholder="Harga satuan" class="form-control @error("item_set") is-invalid @enderror" required /></td><td><button type="button" class="btn btn-danger remove-tr"><i class="fas fa-minus"></i></button></td></tr>';
                        $("#dynamicTable").append(tableDetail);
                    });
                    //View
                    $('#perusahaan-mv').html(invoice['perusahaan']['nama']);
                    $('#nomor-mv').html(invoice['invoice']);
                    $('#cp-mv').html(invoice['contact']['nama'] + " - " + invoice['contact']['no_hp']);
                    $('#keterangan-mv').html(invoice['keterangan']);
                    $('#tanggal-mv').html(invoice['tanggal']);
                    $('#tenggat-mv').html(invoice['tenggat']);
                    $('#terbilang-mv').html(invoice['terbilang']);
                    // console.log(invoice['item_set']);
                    invoice['item_set'].forEach(function(i) {
                        let sub_total = i['harga'] * i['jumlah'];
                        var curr_1 = new Intl.NumberFormat('id-ID', {
                            style: 'currency',
                            currency: 'IDR'
                        }).format(i['harga']);
                        var curr_2 = new Intl.NumberFormat('id-ID', {
                            style: 'currency',
                            currency: 'IDR'
                        }).format(i['sub_total']);
                        $('#detail-invoice').append("<tr class='text-center'>");
                        $('#detail-invoice').append("<td>" + i['nama'] + "</td>");
                        $('#detail-invoice').append("<td class='text-center'>" + i['satuan'] + "</td>");
                        $('#detail-invoice').append("<td class='text-center'>" + i['jumlah'] + "</td>");
                        $('#detail-invoice').append("<td class='text-right'>" + curr_1 + "</td>");
                        $('#detail-invoice').append("<td class='text-right'>" + curr_2 + "</td>");
                        $('#detail-invoice').append("</tr>");
                        total = total + i['sub_total'];
                    });
                    var curr_3 = new Intl.NumberFormat('id-ID', {
                        style: 'currency',
                        currency: 'IDR'
                    }).format(invoice['total']);
                    $('#detail-invoice').append("<tr>");
                    $('#detail-invoice').append("<td></td>");
                    $('#detail-invoice').append("<td></td>");
                    $('#detail-invoice').append("<td></td>");
                    $('#detail-invoice').append("<td class='font-weight-bold text-center'>Total</td>");
                    $('#detail-invoice').append("<td class='text-right'>" + curr_3 + "</td>");
                    $('#detail-invoice').append("</tr>");
                    $('#nominal-mv').html("<i>" + curr_3 + "</i>");
                }
            }
        });
    }


    $(document).ready(function() {

        $('#invoice-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: false,
            ajax: pwd + 'json',
            columnDefs: [{
                targets: [0, 2, 3, 4, 5],
                className: 'text-nowrap'
            }],
            columns: [{
                    data: 'invoice',
                    name: 'invoice'
                },
                {
                    data: 'tanggal',
                    name: 'tanggal'
                },
                {
                    data: 'tenggat',
                    name: 'tenggat',
                    render: function(data, type, row, meta) {
                        if (!data) {
                            return '-'
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'keterangan',
                    name: 'keterangan',
                    className: 'text-left text-wrap'
                },
                {
                    data: 'contact',
                    defaultContent: '-',
                    type: 'html'
                },
                {
                    data: 'aksi',
                    name: 'aksi'
                },
            ]
        });

        $("#status-me").css('width', '100%');
        $("#status-me").select2();
        $("#tanggal-invoice").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        });
        $("#tenggat-invoice").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: 'bottom left',
        });

    });
</script>
@stop
