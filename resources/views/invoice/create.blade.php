@extends('adminlte::page')

@section('title', 'Invoice - Sistem Administrasi Internal PT KKU')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>Invoice</h1>
@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/css/bootstrap-tagsinput.css">
<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

    .bootstrap-tagsinput {
        width: 100%;
    }
</style>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs" id="invoice-list" role="tablist">
                        <li class="nav-item">
                            <a id="nav-1" class="nav-link active" href="#form1" role="tab" aria-controls="form1">Invoice</a>
                        </li>
                        <li class="nav-item">
                            <a id="nav-2" class="nav-link" href="#form2" role="tab" aria-controls="form2">Item</a>
                        </li>
                    </ul>
                </div>
                <form id="form-create" method="POST" action="store">
                    <div class="card-body">
                        @csrf
                        <div class="tab-content">
                            <div class="tab-pane active" id="form1" role="tabpanel">
                                <div class="form-group row">
                                    <label for="cp" class="col-md-4 col-form-label text-md-right">{{ __('Contact Person') }}</label>

                                    <div class="col-md-6">
                                        <select id="cp" type="text" class="form-control @error('cp') is-invalid @enderror" name="cp" required autocomplete="cp" value="">
                                        </select>

                                        <div id="perusahaan"></div>

                                        @error('cp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="invoice" class="col-md-4 col-form-label text-md-right">{{ __('Nomor Invoice') }}</label>

                                    <div class="col-md-6">
                                        <input id="invoice" type="text" class="form-control @error('invoice') is-invalid @enderror" name="invoice" autocomplete="invoice">

                                        @error('invoice')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="keterangan" class="col-md-4 col-form-label text-md-right">{{ __('Keterangan') }}</label>

                                    <div class="col-md-6">
                                        <textarea id="keterangan" rows="5" class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" required autocomplete="keterangan"></textarea>

                                        @error('keterangan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="tanggal" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Invoice') }}</label>

                                    <div class="col-md-6">
                                        <input id="tanggal" type="text" value="" class="form-control @error('tanggal') is-invalid @enderror select-date" name="tanggal" autocomplete="tanggal">

                                        @error('tanggal')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="tenggat" class="col-md-4 col-form-label text-md-right">{{ __('Jatuh Tempo') }}</label>

                                    <div class="col-md-6">
                                        <input id="tenggat" type="text" value="" class="form-control @error('tenggat') is-invalid @enderror select-date" name="tenggat" autocomplete="tenggat">

                                        @error('tenggat')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row" hidden>
                                    <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('User') }}</label>

                                    <div class="col-md-6">
                                        <input id="user" type="text" value="{{ Auth::user()->id }}" class="form-control @error('user') is-invalid @enderror" name="user" required autocomplete="user">

                                        @error('user')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="tags" class="col-md-4 col-form-label text-md-right">{{ __('Tags') }}</label>

                                    <div class="col-md-6">
                                        <input id="tags" data-role="tagsinput" type="text" class="form-control @error('tags') is-invalid @enderror" name="tags" autocomplete="tags">

                                        @error('tags')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="form2" role="tabpanel">
                                <div class="form-group row">
                                    <table class="table text-center" id="dynamicTable">
                                        <tr>
                                            <th>Nama</th>
                                            <th>Satuan</th>
                                            <th>Jumlah</th>
                                            <th>Harga</th>
                                            <th>Tambah/Hapus</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" id="item_set[0][nama]" name="item_set[0][nama]" placeholder="Nama item" class="form-control  @error('item_set') is-invalid @enderror" required>
                                            </td>
                                            <td>
                                                <input type="text" id="item_set[0][satuan]" name="item_set[0][satuan]" placeholder="Satuan" class="form-control @error('item_set') is-invalid @enderror" required>
                                            </td>
                                            <td>
                                                <input type="number" id="item_set[0][jumlah]" name="item_set[0][jumlah]" placeholder="Jumlah" class="form-control @error('item_set') is-invalid @enderror" required>
                                            </td>
                                            <td>
                                                <input type="number" id="item_set[0][harga]" name="item_set[0][harga]" placeholder="Harga satuan" class="form-control @error('item_set') is-invalid @enderror" required>
                                            </td>
                                            <td>
                                                <button type="button" name="add" id="add" class="btn btn-success"><i class="fas fa-plus"></i></button>
                                            </td>

                                            @error('item_set')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <button type="submit" id="send" class="btn btn-primary">
                                {{ __('Tambah') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/bootstrap-tagsinput.js"></script>
<script>
    var p = "#perusahaan";
    var cp = "#cp";
    var total_cat = '{{ \App\Invoice::count() }}';
    var i = 0

    $("#add").click(function() {
        i = i + 1;
        $("#dynamicTable").append('<tr><td><input type="text" id="item_set[' + i + '][nama]" name="item_set[' + i + '][nama]" placeholder="Nama item" class="form-control @error("item_set") is-invalid @enderror nama_item" required /></td><td><input type="text" id="item_set[' + i + '][satuan]" name="item_set[' + i + '][satuan]" placeholder="Satuan" class="form-control @error("item_set") is-invalid @enderror satuan_item" required /></td><td><input type="number" id="item_set[' + i + '][jumlah]" name="item_set[' + i + '][jumlah]" placeholder="Jumlah" class="form-control @error("item_set") is-invalid @enderror jumlah_item" required /></td><td><input type="number" id="item_set[' + i + '][harga]" name="item_set[' + i + '][harga]" placeholder="Harga satuan" class="form-control @error("item_set") is-invalid @enderror harga_item" required /></td><td><button type="button" class="btn btn-danger remove-tr"><i class="fas fa-minus"></i></button></td></tr>');

    });

    $(document).on('click', '.remove-tr', function() {
        $(this).parents('tr').remove();
    });

    $('#invoice-list a').on('click', function(e) {
        e.preventDefault()
        $(this).tab('show')
    });

    $("button#send").click(function() {
        var nama = $("#item_set[0][nama]").val();
        var contact = $("#cp option:selected").val();
        var ket = $("#keterangan").val();
        if ((contact && ket) && !nama) {
            $("#nav-1").removeClass("active");
            $("#form1").removeClass("active");
            $("#nav-2").addClass("active");
            $("#form2").addClass("active");
        }
    });

    $(document).ready(function() {
        $("#form-create").submit(function() {
            $("#send").attr("disabled", true);
            return true;
        });

        $(p).css('width', '100%');
        $(cp).css('width', '100%');
        $(cp).select2({
            placeholder: 'Pilih Contact Person',
            minimumInputLength: 3,
            ajax: {
                url: '{{ route("api.contact.search") }}',
                dataType: 'json',
            }
        });

        $(cp).on('change', function() {
            var pid = $("#cp option:selected").attr("title");
            var cpid = $("#cp option:selected").attr("value");
            $(p).html('<input hidden type="text" name="perusahaans" value="' + pid + '">');
        });

        for (let x = 1; x <= total_cat; x++) {
            $(p.concat("-" + x)).css('width', '100%');
            $(p.concat("-" + x)).select2();
        }

        $('.select-date').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "bottom left"
        });
    });
</script>
@stop