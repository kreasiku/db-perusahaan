@extends('adminlte::page')

@section('title', 'Manajemen Anggota')

@php
config(['adminlte.logo' => Auth::user()->name]);
config(['adminlte.logo_img' => Auth::user()->avatar]);
@endphp

@section('content_header')
<h1>Manajemen Anggota</h1>
@stop

@section('css')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

    th {
        text-align: center !important;
    }

    td {
        vertical-align: middle !important;
    }

    .word-wrap {
        word-break: break-all;
        /* word-wrap: break-word; */
        /* overflow-wrap: break-word; */
    }

    .no-wrap {
        white-space: nowrap;
    }

    .fixed {
        table-layout: fixed;
    }

    .card-header .navbar {
        height: 40px;
        flex-wrap: nowrap;
    }
</style>
@stop

@section('content')
<!-- Message -->
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@elseif ($message = Session::get('failed'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@endif
<!-- Message -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="navbar" id="card-header">
                    <h3 class="card-title align-middle">Daftar Anggota</h3>
                    <div class="navbar d-flex card-tools">
                        <a class="btn btn-sm btn-outline-secondary bd-highlight" href="{{ route('anggota.create') }}"><i class="fas fa-plus"></i> Tambah</a>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table">
                <table class="table table-hover dataTables" id="anggota-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Divisi</th>
                            <th>Role</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- /.Modal View -->
                        <div class="modal fade view-anggota">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-success">
                                        <h4 class="modal-title">Detail Anggota</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Nama ') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="name-mv"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Email') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="email-mv"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Divisi') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="divisi-mv"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Role') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="role-mv"></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default float-right" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>

                        <!-- Modal Edit -->
                        <div class="modal fade edit-anggota">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h4 class="modal-title">Ubah Data Anggota</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form id="form-me" method="POST" action="#">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <div class="modal-body">
                                            <input id="id-me" type="hidden" class="form-control" name="uid" autocomplete="uid" autofocus>

                                            <div class="form-group row">
                                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                                <div class="col-md-6">
                                                    <input id="name-me" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="" autocomplete="name" autofocus>

                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                                <div class="col-md-6">
                                                    <input id="email-me" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value=""" required autocomplete="email">

                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="password-me" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="" placeholder="Masukkan Password Baru" autocomplete="new-password">

                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="divisi" class="col-md-4 col-form-label text-md-right">{{ __('Divisi') }}</label>

                                                <div class="col-md-6">
                                                    <input id="divisi-me" type="text" class="form-control @error('divisi') is-invalid @enderror" name="divisi" value="" required autocomplete="divisi" autofocus>

                                                    @error('divisi')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="is_admin" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>

                                                <div class="col-md-6">
                                                    <select id="role-me" type="text" class="js-example-basic-single form-control @error('is_admin') is-invalid @enderror" name="is_admin" required autocomplete="is_admin">
                                                    </select>

                                                    @error('is_admin')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal edit end -->

                        <!-- /.Modal Delete -->
                        <div class="modal fade delete-anggota">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h4 class="modal-title">Apakah Anda yakin ingin menghapus anggota berikut?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="container">
                                            <div class="row">
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Nama ') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="name-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Email') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="email-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Role') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="role-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Divisi') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="divisi-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Jumlah Pekerjaan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="count-md"></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="modal-footer">
                                            <form id="form-md" action="">
                                                <input id="id-md" name="id" type="hidden">
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@stop

@section('js')
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script>
    var pwd = window.location.pathname;
    pwd = pwd.substring(0, pwd.lastIndexOf('/'));
    if (pwd.length > 1) {
        pwd += "/";
    } else {
        pwd += "anggota/";
    }

    console.log(pwd);
    function buttonClick(id) {
        $('#id-me').val("");
        $('#id-md').val("");

        $("#name-mv").html("");
        $("#email-mv").html("");
        $("#divisi-mv").html("");

        $("#name-me").val("");
        $("#email-me").val("");
        $("#divisi-me").val("");

        $("#name-md").html("");
        $("#email-md").html("");
        $("#divisi-md").html("");
        $("#role-me").html("");

        $("#count-md").html("");
        console.log(id);
        $.ajax({
            url: '{{ route("anggota.getJson") }}',
            dataType: 'json',
            data: {
                id: id
            },
            success: function(data) {
                if(data){
                    var user = data;
                    console.log(user);
                    $("#form-me").attr("action", pwd + "update/" + id);
                    $("#form-md").attr("action", pwd + "delete/" + id);
                    $('#id-me').val(user['id']);
                    $('#id-md').val(user['id']);

                    $("#name-mv").html(user['name']);
                    $("#email-mv").html(user['email']);
                    $("#divisi-mv").html(user['divisi']);

                    $("#name-me").val(user['name']);
                    $("#email-me").val(user['email']);
                    $("#divisi-me").val(user['divisi']);

                    $("#name-md").html(user['name']);
                    $("#email-md").html(user['email']);
                    $("#divisi-md").html(user['divisi']);

                    $("#count-md").html(user['count']);
                    if (user['is_admin']) {
                        $("#role-mv").html('<span class="badge badge-success">Admin</span>');
                        $("#role-md").html('<span class="badge badge-success">Admin</span>');
                        $("#role-me").append('<option value="0">User</option>');
                        $("#role-me").append('<option selected value="'+user['is_admin']+'">Admin</option>');
                    } else {
                        $("#role-mv").html('<span class="badge badge-info">User</span>');
                        $("#role-md").html('<span class="badge badge-info">User</span>');
                        $("#role-me").append('<option value="1">Admin</option>');
                        $("#role-me").append('<option selected value="'+user['is_admin']+'">User</option>');
                    }
                }
            }
        });
    }

    $(document).ready(function() {
        $("#anggota-table").DataTable({
            serverSide : true,
            processing : true,
            ajax : pwd + "anggota_json",
            columns :[
                {data: 'created_at',
                render:
                    function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                className:'text-center'
                },
                {data:'name', name:'name', className:'text-center'},
                {data:'divisi', name:'divisi', className:'text-center'},
                {data:'is_admin', className: 'text-center',
                render: 
                    function (data, type, row, meta) {
                        if (data == 0) {
                            return '<span class="badge badge-info">User</span>';
                        } else {
                            return '<span class="badge badge-success">Admin</span>'
                        }
                    }
                },
                {data:'aksi', name:'aksi', className:'text-center'}
            ]
        });

        $("button.close").click(function () {
            $("#form-me").attr("action", "#");
            $("#form-md").attr("action", "#");
        });
    });
</script>
@stop
