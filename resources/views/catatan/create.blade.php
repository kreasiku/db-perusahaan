@extends('adminlte::page')

@section('title', 'Manajemen Catatan - Sistem Administrasi Internal PT KKU')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>Manajemen Catatan</h1>
@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }
</style>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Tambahkan Catatan') }}</div>
                <form id="form-create" method="POST" action="store">
                    <div class="card-body">
                        @csrf
                        <div class="form-group row">
                            <label for="cp" class="col-md-4 col-form-label text-md-right">{{ __('Contact Person') }}</label>

                            <div class="col-md-6">
                                <select id="cp" type="text" class="form-control @error('cp') is-invalid @enderror" name="cp" required autocomplete="cp" autofocus value="">
                                </select>

                                <div id="perusahaan"></div>

                                @error('cp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="catatans" class="col-md-4 col-form-label text-md-right">{{ __('Catatan') }}</label>

                            <div class="col-md-6">
                                <textarea id="catatans" rows="5" class="form-control @error('catatans') is-invalid @enderror" name="catatans" required autocomplete="catatans" autofocus></textarea>

                                @error('catatans')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                            <div class="col-md-6">
                                <select name="status" id="status" required class="form-control @error('status') is-invalid @enderror" autocomplete="status" autofocus>
                                    @foreach ($sc as $s)
                                    <option value="{{ $s->id }}">{{ $s->status }}</option>
                                    @endforeach
                                </select>

                                @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="follow_up" class="col-md-4 col-form-label text-md-right">{{ __('Follow Up') }}</label>

                            <div class="col-md-6">
                                <input id="follow_up" type="text" value="" class="form-control @error('follow_up') is-invalid @enderror" name="follow_up" autocomplete="follow_up">

                                @error('follow_up')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" hidden>
                            <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('User') }}</label>

                            <div class="col-md-6">
                                <input id="user" type="text" value="{{ Auth::user()->id }}" class="form-control @error('user') is-invalid @enderror" name="user" required autocomplete="user">

                                @error('user')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <button type="submit" id="send" class="btn btn-primary">
                                {{ __('Tambah') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="/js/bootstrap-datepicker.min.js"></script>
<script>
    var stat = "#status";
    var p = "#perusahaan";
    var cp = "#cp";
    var total_cat = '{{ \App\Catatan::count() }}';

    $(document).ready(function() {
        $("#form-create").submit(function () {
            $("#send").attr("disabled", true);
            return true;
        });

        $(stat).css('width', '100%');
        $(p).css('width', '100%');
        $(cp).css('width', '100%');
        $(stat).select2({
            placeholder: 'Pilih status'
        });
        $(cp).select2({
            placeholder: 'Pilih staff',
            minimumInputLength: 3,
            ajax: {
                url: '{{ route("api.contact.search") }}',
                dataType: 'json',
            }
        });

        $(cp).on('change', function() {
            var pid = $("#cp option:selected").attr("title");
            var cpid = $("#cp option:selected").attr("value");
            $(p).html('<input hidden type="text" name="perusahaans" value="' + pid + '">');
        });

        for (let x = 1; x <= total_cat; x++) {
            $(p.concat("-" + x)).css('width', '100%');
            $(p.concat("-" + x)).select2();
        }

        $('#follow_up').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "bottom left"
        });
    });
</script>
@stop
