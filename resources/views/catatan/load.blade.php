<div class="card">
    <div class="card-header">
        <div class="navbar" id="card-header">
            <h3 class="card-title align-middle">Daftar Catatan</h3>
            <div class="navbar d-flex card-tools">
                <a class="btn btn-sm btn-outline-secondary bd-highlight" href="{{ route('catatan.create') }}"><i class="fas fa-plus"></i> Tambah</a>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="sorting" data-sorting_type="asc" data-column_name="createdAt" style="cursor: pointer">Dibuat <span id="id_icon"></span></th>
                    <th class="sorting" data-sorting_type="asc" data-column_name="catatan" style="cursor: pointer">Catatan <span id="catatan_icon"></th>
                    <th class="sorting" data-sorting_type="asc" data-column_name="status" style="cursor: pointer">Status <span id="status_icon"></th>
                    <th class="sorting" data-sorting_type="asc" data-column_name="followUp" style="cursor: pointer">Follow Up <span id="followUp_icon"></th>
                    <th class="sorting" data-sorting_type="asc" data-column_name="cp" style="cursor: pointer">CP <span id="cp_icon"></th>
                    <th class="sorting" data-sorting_type="asc" data-column_name="createdAt" style="cursor: pointer">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php $num=0; @endphp
                @foreach($catatan as $c)
                <tr>
                    <td class="text-center">
                        @php
                        echo date_format($c->created_at,'Y-m-d');
                        @endphp
                    </td>
                    <td class="word-wrap">{{$c->catatans}}</td>

                    @foreach ($sc as $s)
                    @if ($c->status == $s->id)
                    <td class="no-wrap align-middle text-center project-state">
                        @if ($s->id == 1)
                        <span class="badge badge-info">{{$s->status}}</span>
                        @elseif ($s->id == 2)
                        <span class="badge badge-danger">{{$s->status}}</span>
                        @elseif ($s->id == 3)
                        <span class="badge badge-success">{{$s->status}}</span>
                        @endif
                    </td>
                    @endif
                    @endforeach

                    <td class="text-center no-wrap">{{$c->follow_up}}</td>
                    <td class="no-wrap">
                        @if ($c->contact)
                        {{$c->contact->nama}} - {{$c->contact->no_hp}} <br>
                        @else
                        <i class="text-danger">CP Tidak tersedia</i> <br>
                        @endif

                        @if ($c->perusahaan)
                        {{ $c->perusahaan->nama }}
                        @else
                        <i class="text-danger">Perusahaan tidak tersedia</i>
                        @endif
                    </td>
                    <td class="text-center no-wrap">
                        <button class="btn btn-success btn-sm catatan" allid="{{$c->id}}-{{$c->perusahaans}}-{{$c->cp}}-{{$c->status}}" data-toggle="modal" data-target=".view-catatan">
                            <i class="fas fa-eye"></i> Lihat
                        </button>
                        <button class="btn btn-info btn-sm catatan" allid="{{$c->id}}-{{$c->perusahaans}}-{{$c->cp}}-{{$c->status}}" data-toggle="modal" data-target=".edit-catatan">
                            <i class="fas fa-edit"></i> Ubah
                        </button>
                        <button class="btn btn-danger btn-sm catatan" allid="{{$c->id}}-{{$c->perusahaans}}-{{$c->cp}}-{{$c->status}}" data-toggle="modal" data-target=".delete-catatan">
                            <i class="fas fa-trash"></i> Hapus
                        </button>
                    </td>
                </tr>
                @endforeach

                <!-- Modal View -->
                <div class="modal fade view-catatan">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-success">
                                <h4 class="modal-title">Detail Catatan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col text-md-left col-form-label col-md-4">{{ __('Perusahaan') }}</div>
                                        <div class="col text-md-left col-form-label col-md-4" id="perusahaan-mv"></div>

                                        <div class="w-100"></div>
                                        <div class="col text-md-left col-form-label col-md-4">{{ __('Catatan') }}</div>
                                        <div class="col text-md-left col-form-label col-md-4" id="catatan-mv"></div>
                                        <div class="w-100"></div>
                                        <div class="col text-md-left col-form-label col-md-4">{{ __('Status') }}</div>

                                        <div class="col text-md-left col-form-label col-md-4" id="status-mv"></div>

                                        <div class="w-100"></div>
                                        <div class="col text-md-left col-form-label col-md-4">{{ __('Follow up') }}</div>
                                        <div class="col text-md-left col-form-label col-md-4" id="follow_up-mv"></div>
                                        <div class="w-100"></div>
                                        <div class="col text-md-left col-form-label col-md-4">{{ __('Contact Person') }}</div>
                                        <div class="col text-md-left col-form-label col-md-4" id="cp-mv"></div>
                                        <div class="w-100"></div>
                                        <div class="col text-md-left col-form-label col-md-4">{{ __('Diperbarui pada') }}</div>
                                        <div class="col text-md-left col-form-label col-md-4" id="updated_at-mv"></div>
                                    </div>
                                </div>
                                <br>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default float-right" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                </div>
                <!-- /.modal -->

                <!-- Modal Edit -->
                <div class="modal fade edit-catatan">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-info">
                                <h4 class="modal-title">Ubah Catatan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form id="form-me" method="POST" action="#">
                                @csrf
                                @method('PUT')
                                <div class="modal-body">
                                    <input id="id-me" hidden type="text" class="form-control" name="form-id" required autocomplete="form-id" autofocus>

                                    <div class="form-group row">
                                        <label for="cp" class="col-md-4 col-form-label text-md-right">{{ __('Contact Perusahaan') }}</label>

                                        <div class="col-md-6">
                                            <input id="cp-me" readonly type="text" class="form-control @error('form-cp') is-invalid @enderror" name="form-cp" required autocomplete="form-cp" autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="catatans" class="col-md-4 col-form-label text-md-right">{{ __('Catatan') }}</label>

                                        <div class="col-md-6">
                                            <textarea id="catatan-me" rows="5" class="form-control @error('catatans') is-invalid @enderror" name="catatans" required autocomplete="catatans" autofocus></textarea>

                                            @error('catatans')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                                        <div class="col-md-6">
                                            <select name="status" id="status-me" required class="select2 form-control @error('status') is-invalid @enderror" autocomplete="status" autofocus>
                                            </select>

                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="follow_up" class="col-md-4 col-form-label text-md-right">{{ __('Follow Up') }}</label>

                                        <div class="col-md-6">
                                            <input id="follow_up-me" type="text" class="form-control @error('follow_up') is-invalid @enderror" name="follow_up" autocomplete="follow_up">

                                            @error('follow_up')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row" hidden>
                                        <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('User') }}</label>

                                        <div class="col-md-6">
                                            <input id="user" type="text" value="{{ Auth::user()->id }}" class="form-control @error('user') is-invalid @enderror" name="user" required autocomplete="user">

                                            @error('user')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                                <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal edit end -->

                <!-- Modal Delete -->
                <div class="modal fade delete-catatan">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-danger">
                                <h4 class="modal-title">Apakah Anda yakin ingin menghapus Catatan berikut?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col text-md-left col-form-label col-md-4">{{ __('Catatan') }}</div>
                                        <div class="col text-md-left col-form-label col-md-4" id="catatan-md"></div>
                                        <div class="w-100"></div>
                                        <div class="col text-md-left col-form-label col-md-4">{{ __('Perusahaan') }}</div>
                                        <div class="col text-md-left col-form-label col-md-4" id="perusahaan-md"></div>
                                        <div class="w-100"></div>
                                        <div class="col text-md-left col-form-label col-md-4">{{ __('Status') }}</div>
                                        <div class="col text-md-left col-form-label col-md-4" id="status-md"></div>
                                        <div class="w-100"></div>
                                        <div class="col text-md-left col-form-label col-md-4">{{ __('Contact Person') }}</div>
                                        <div class="col text-md-left col-form-label col-md-4" id="cp-md"></div>
                                    </div>
                                </div>
                                <br>
                                <div class="modal-footer">
                                    <form id="form-md" action="">
                                        <input id="id-md" name="id" hidden>
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                </div>
                <!-- /.modal -->
            </tbody>
        </table>
        <input type="hidden" name="hidden_page" id="hidden_page" value="1">
        <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="id">
        <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc">
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <div class="card-tools">
            <div class="pagination-sm float-right">{!! $catatan->links() !!}</div>
        </div>
    </div>
</div>