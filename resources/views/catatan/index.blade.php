@extends('adminlte::page')

@section('title', 'Manajemen Catatan - Sistem Administrasi Internal PT KKU')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>Manajemen Catatan</h1>
@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

    th {
        text-align: center !important;
    }

    td {
        vertical-align: middle !important;
    }

    .word-wrap {
        word-break: break-all;
        /* word-wrap: break-word; */
        /* overflow-wrap: break-word; */
    }

    .no-wrap {
        white-space: nowrap;
    }

    .fixed {
        table-layout: fixed;
    }

    .card-header .navbar {
        height: 40px;
        flex-wrap: nowrap;
    }
</style>
@stop

@section('content')
<!-- Message -->
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@elseif ($message = Session::get('failed'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@endif
<!-- Message -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="navbar" id="card-header">
                    <h3 class="card-title align-middle">Daftar Catatan</h3>
                    <div class="navbar d-flex card-tools">
                        <a class="btn btn-sm btn-outline-secondary bd-highlight" href="{{ route('catatan.create') }}"><i class="fas fa-plus"></i> Tambah</a>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
                <table class="table table-hover dataTables text-center" id="catatan-table">
                    <thead class="p-0">
                        <tr>
                            <th>Dibuat</th>
                            <th class="text-center">Catatan</th>
                            <th>Status</th>
                            <th>Follow Up</th>
                            <th>Contact<br>Perusahaan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="p-0">

                        <!-- Modal View -->
                        <div class="modal fade view-catatan">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-success">
                                        <h4 class="modal-title">Detail Catatan</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Perusahaan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="perusahaan-mv"></div>

                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Catatan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="catatan-mv"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Status') }}</div>

                                                <div class="col text-md-left col-form-label col-md-4" id="status-mv"></div>

                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Follow up') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="follow_up-mv"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Contact Person') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="cp-mv"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Diperbarui pada') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="updated_at-mv"></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default float-right" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <!-- /.modal -->

                        <!-- Modal Edit -->
                        <div class="modal fade edit-catatan">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h4 class="modal-title">Ubah Catatan</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form id="form-me" method="POST" action="#">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <input id="id-me" hidden type="text" class="form-control" name="form-id" required autocomplete="form-id" autofocus>

                                            <div class="form-group row">
                                                <label for="cp" class="col-md-4 col-form-label text-md-right">{{ __('Contact Perusahaan') }}</label>

                                                <div class="col-md-6">
                                                    <input id="cp-me" readonly type="text" class="form-control @error('form-cp') is-invalid @enderror" name="form-cp" required autocomplete="form-cp" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="catatans" class="col-md-4 col-form-label text-md-right">{{ __('Catatan') }}</label>

                                                <div class="col-md-6">
                                                    <textarea id="catatan-me" rows="5" class="form-control @error('catatans') is-invalid @enderror" name="catatans" required autocomplete="catatans" autofocus></textarea>

                                                    @error('catatans')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                                                <div class="col-md-6">
                                                    <select name="status" id="status-me" required class="select2 form-control @error('status') is-invalid @enderror" autocomplete="status" autofocus>
                                                    </select>

                                                    @error('status')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="follow_up" class="col-md-4 col-form-label text-md-right">{{ __('Follow Up') }}</label>

                                                <div class="col-md-6">
                                                    <input id="follow_up-me" type="text" class="form-control @error('follow_up') is-invalid @enderror" name="follow_up" autocomplete="follow_up">

                                                    @error('follow_up')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row" hidden>
                                                <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('User') }}</label>

                                                <div class="col-md-6">
                                                    <input id="user" type="text" value="{{ Auth::user()->id }}" class="form-control @error('user') is-invalid @enderror" name="user" required autocomplete="user">

                                                    @error('user')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>

                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal edit end -->

                        <!-- Modal Delete -->
                        <div class="modal fade delete-catatan">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h4 class="modal-title">Apakah Anda yakin ingin menghapus Catatan berikut?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Catatan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="catatan-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Perusahaan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="perusahaan-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Status') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="status-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Contact Person') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="cp-md"></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="modal-footer">
                                            <form id="form-md" action="">
                                                <input id="id-md" name="id" hidden>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <!-- /.modal -->

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->

        </div>
        <!-- /.card -->
    </div>
</div>
@stop

@section('js')
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="/js/dataTables-num-html.js"></script>
<script>
    var pwd = window.location.pathname;
    pwd = pwd.substring(0, pwd.lastIndexOf('/'));
    if (pwd.length > 1) {
        pwd += "/";
    } else {
        pwd += "catatan/";
    }

    function buttonClick(id, sid) {
        var sc = @JSON($sc);
        console.log(pwd);
        console.log(id);

        $("#catatan-mv").html("");
        $("#catatan-me").html("");
        $("#catatan-md").html("");

        $("#perusahaan-mv").html("");
        $("input#perusahaan-me").val("");
        $("#perusahaan-md").html("");

        $("#status-mv").html("");
        $("#status-me").html("");
        $("#status-md").html("");

        $("#follow_up-mv").html("");
        $("#follow_up-me").val("");
        $("#follow_up-md").html("");

        $("#cp-mv").html("");
        $("#cp-me").val("");
        $("#cp-md").html("");

        $("#updated_at-mv").html("");

        console.log(pwd);

        $.ajax({
            url: '{{ route("catatan.getJson") }}',
            dataType: 'json',
            data: {
                id: id,
                status: sid
            },
            success: function(data) {
                if (data) {
                    var catatan = data;
                    console.log(data);
                    $("#form-me").attr("action", pwd + "update/" + id);
                    $("#form-md").attr("action", pwd + "delete/" + id);

                    $('#id-me').val(catatan['id']);
                    $('#id-md').val(id);

                    // Catatan
                    $("#catatan-mv").html(catatan['catatans']);
                    $("#catatan-me").html(catatan['catatans']);
                    $("#catatan-md").html(catatan['catatans']);

                    // Status
                    if (sid == 1) {
                        $("#status-mv").html('<span class="badge badge-info">Sedang Diproses</span>');
                        $("#status-md").html('<span class="badge badge-info">Sedang Diproses</span>');
                    } else if (sid == 2) {
                        $("#status-mv").html('<span class="badge badge-danger">Gagal</span>');
                        $("#status-md").html('<span class="badge badge-danger">Gagal</span>');
                    } else {
                        $("#status-mv").html('<span class="badge badge-success">Selesai</span>');
                        $("#status-md").html('<span class="badge badge-success">Selesai</span>');
                    }

                    sc.forEach(function(s) {
                        console.log(s['status']);
                        if (s['id'] == sid) {
                            $("#status-me").append('<option selected value="' + s['id'] + '"> ' + s['status'] + ' </option>');
                        } else {
                            $("#status-me").append('<option value="' + s['id'] + '"> ' + s['status'] + ' </option>');
                        }
                    });

                    // Follow_up
                    console.log(catatan['follow_up']);
                    $("#follow_up-mv").html(catatan['follow_up']);
                    $("#follow_up-me").val(catatan['follow_up']);
                    $("#follow_up-md").html(catatan['follow_up']);

                    // Contact
                    console.log(catatan['cp']);
                    if (catatan['cp'] != null) {
                        $("#cp-mv").html(catatan['contact']['nama']);
                        $("#cp-me").val(catatan['contact']['nama']);
                        $("#cp-md").html(catatan['contact']['nama']);
                    } else {
                        $("#cp-mv").html("");
                        $("#cp-me").val("");
                        $("#cp-md").html("");
                    }

                    console.log(catatan['perusahaan'])
                    if (catatan['perusahaan'] != null) {
                        $("#cp-mv").append(' - ' + catatan['perusahaan']['nama']);
                        $("#cp-me").val(function() {
                            return this.value + ' - ' + catatan['perusahaan']['nama'];
                        });
                        $("#cp-md").append(' - ' + catatan['perusahaan']['nama']);

                        $("#perusahaan-mv").html(catatan['perusahaan']['nama']);
                        $("#perusahaan-md").html(catatan['perusahaan']['nama']);
                    } else {
                        $("#cp-mv").append(' - ' + catatan['perusahaans']);
                        $("#cp-me").val(function() {
                            return this.value + ' - ' + catatan['perusahaans'];
                        });
                        $("#cp-md").append(' - ' + catatan['perusahaans']);

                        $("#perusahaan-mv").html(catatan['perusahaans']);
                        $("#perusahaan-md").html(catatan['perusahaans']);
                    }

                    // Diupdate
                    console.log(catatan['updated_at']);
                    $("#updated_at-mv").html(catatan['updated_at']);

                }
            }
        });
    }


    $(document).ready(function() {
        var pwd = window.location.pathname;
        pwd = pwd.substring(0, pwd.lastIndexOf('/'));
        if (pwd.length > 1) {
            pwd += "/";
        } else {
            pwd += "catatan/";
        }
        console.log(pwd);
        $('#catatan-table').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": false,
            ajax: pwd + 'json',
            columnDefs: [{
                    targets: [0, 2, 3, 4, 5],
                    className: 'text-nowrap'
                }
            ],
            columns: [{
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'catatans',
                    name: 'catatans',
                    className: 'text-left text-wrap'
                },
                {
                    data: 'status',
                    name: 'status',
                    render: function(data, type, row, meta) {
                        if (data == 1) {
                            return '<span class="badge badge-info">Sedang Diproses</span>';
                        } else if (data == 2) {
                            return '<span class="badge badge-danger">Gagal</span>';
                        } else {
                            return '<span class="badge badge-success">Selesai</span>'
                        }
                    }
                },
                {
                    data: 'follow_up',
                    name: 'follow_up'
                },
                {
                    data: 'contact',
                    defaultContent: '-',
                    type: 'html'
                },
                {
                    data: 'aksi',
                    name: 'aksi'
                },
            ]
        });

        $("#status-me").css('width', '100%');
        $("#status-me").select2();
        $("#follow_up-me").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

    });
</script>
@stop
