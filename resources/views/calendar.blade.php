<!DOCTYPE html>
<html>

<head>
    <title>Fullcalender</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"
    integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>

<body>
    <div class="container">
        <div class="response"></div>
        <div id='calendar'></div>
    </div>
</body>

<script>
$(document).ready(function () {
    
    var SITEURL = "{{url('/')}}";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });

    Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();
    var start = " ";
    var end = " ";

    return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
            ].join('-');
    };

    var date = new Date();

    var calendar = $('#calendar').fullCalendar({
        headers: {
            left: 'prev,next,today,month,agendaWeek',
            center: '',
            right: 'title'
        },
        defaultDate: Date(),
        editable: true,
        showNonCurrentDates : false,
        events: {
            url: "{{ route('calendar') }}",
        },
        
        displayEventTime: true,
        eventRender: function (event, element, view) {
            console.log(event);
        }
    });
});
</script>

</html>