@extends('adminlte::page')

@section('title', 'Manajemen Staff Perusahaan - Sistem Administrasi Internal PT KKU')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>Manajemen Staff Perusahaan</h1>
@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

    th {
        text-align: center !important;
    }

    td {
        vertical-align: middle !important;
    }

    .word-wrap {
        word-break: break-all;
        /* word-wrap: break-word; */
        /* overflow-wrap: break-word; */
    }

    .no-wrap {
        white-space: nowrap;
    }

    .fixed {
        table-layout: fixed;
    }

    .card-header .navbar {
        height: 40px;
        flex-wrap: nowrap;
    }
</style>
@stop

@section('content')

<!-- Message -->
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@elseif ($message = Session::get('failed'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{ $message }}</strong>
</div>
@endif
<!-- Message -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="navbar" id="card-header">
                    <h3 class="card-title align-middle">Daftar Staff Perusahaan</h3>
                    <div class="navbar d-flex card-tools">
                        <a class="btn btn-sm btn-outline-secondary bd-highlight" href="{{ route('contact.create') }}"><i class="fas fa-plus"></i> Tambah</a>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
                <table class="table table-hover text-center" id="contact-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Tanggal Lahir</th>
                            <th>Perusahaan</th>
                            <th>Nomor</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- @php $num=0; @endphp
                        @foreach($contact as $ct)
                        <tr>
                            <td class="text-center">{{$num+=1}}</td>
                            <td class="no-wrap">{{$ct->nama}}</td>
                            <td class="no-wrap">{{$ct->email}}</td>
                            <td class="no-wrap text-center">{{$ct->tgl_lahir}}</td>
                            @if ($ct->perusahaan)
                                <td class="no-wrap text-center">{{$ct->perusahaan->nama}}</td>
                            @else
                            <td class="no-wrap text-center"><i class="text-danger">Sudah Dihapus</i></td>
                            @endif
                            <td class="no-wrap text-center">{{$ct->no_hp}}</td>
                            <td class="no-wrap text-center">
                                <a class="btn btn-success btn-sm" href="{{ route('contact.show', $ct->id) }}">
                                    <i class="fas fa-eye"></i> Lihat
                                </a>
                                <button class="btn btn-info btn-sm contact" allid="{{ $ct->id }}-{{ $ct->perusahaans }}" data-toggle="modal" data-target=".edit-contact">
                                    <i class="fas fa-edit"></i> Ubah
                                </button>
                                <button class="btn btn-danger btn-sm contact" allid="{{ $ct->id }}-{{ $ct->perusahaans }}" data-toggle="modal" data-target=".delete-contact">
                                    <i class="fas fa-trash"></i> Hapus
                                </button>
                            </td>
                        </tr>

                        @endforeach --}}
                        <!-- Modal Edit -->
                        <div class="modal fade edit-contact">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h4 class="modal-title">Ubah Staff</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form id="form-me" method="POST" action="#">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <input hidden id="id-me" type="text" class="form-control" name="cid" required autocomplete="cid" autofocus>

                                            <div class="form-group row">
                                                <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>

                                                <div class="col-md-6">
                                                    <input value="" id="nama-me" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="" required autocomplete="nama" autofocus>

                                                    @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                                <div class="col-md-6">
                                                    <input id="email-me" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">

                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="tgl_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Lahir') }}</label>

                                                <div class="col-md-6">
                                                    <input readonly value="" id="tgl_lahir-me" type="text" class="form-control @error('tgl_lahir') is-invalid @enderror" name="tgl_lahir" value="" autocomplete="tgl_lahir">

                                                    @error('tgl_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="perusahaans" class="col-md-4 col-form-label text-md-right">{{ __('Perusahaan') }}</label>

                                                <div class="col-md-6 no-wrap">

                                                    <select id="perusahaan-me" type="text" class="js-example-basic-single form-control @error('perusahaans') is-invalid @enderror" name="perusahaans" required autocomplete="perusahaans">
                                                    </select>

                                                    @error('perusahaans')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="no_hp" class="col-md-4 col-form-label text-md-right">{{ __('Nomor') }}</label>

                                                <div class="col-md-6">
                                                    <input type="text" id="no_hp-me" class="form-control @error('no_hp') is-invalid @enderror" value="" name="no_hp" required autocomplete="no_hp">

                                                    @error('no_hp')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Model Footer -->
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- End of Modal Edit -->

                        <!-- Modal Hapus -->
                        <div class="modal fade delete-contact">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h4 class="modal-title">Apakah Anda yakin ingin menghapus Staff berikut?
                                        </h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Nama Staff') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="nama-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Email Staff') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="email-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('Perusahaan') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="perusahaan-md"></div>
                                                <div class="w-100"></div>
                                                <div class="col text-md-left col-form-label col-md-4">{{ __('No. Telp') }}</div>
                                                <div class="col text-md-left col-form-label col-md-4" id="no_hp-md"></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="modal-footer">
                                            <form id="form-md" action="">
                                                <input id="id-md" name="id" hidden>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <!-- /.modal -->
                        <!-- End of Modal Hapus -->
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@stop

@section('js')
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script>
    var pwd = window.location.pathname;
    pwd = pwd.substring(0, pwd.lastIndexOf('/'));
    if (pwd.length > 1) {
        pwd += "/";
    } else {
        pwd += "contact/";
    }
    function buttonClick(id) {
        $("#nama-me").val("");
        $("#tgl_lahir-me").val("");
        $("#no_hp-me").val("");
        $("#email-me").val("")

        $("#nama-md").html("");
        $("#email-md").html("");
        $("#no_hp-md").html("");

        $("#perusahaan-me").html("");
        $("#perusahaan-md").html("");
        $("#form-me").attr("action", pwd + "update/" + id);
        $("#form-md").attr("action", pwd + "delete/" + id);
        console.log(id);

        $.ajax({
            url: '{{ route("contact.getJson") }}',
            dataType: 'json',
            data: {
                id: id,
            },
            success: function(data) {
                if (data) {
                    console.log(data);
                    var contact = data;
                    $("#id-md").val(contact['id']);
                    $('#id-me').val(contact['id']);

                    $("#nama-me").val(contact['nama']);
                    $("#tgl_lahir-me").val(contact['tgl_lahir']);
                    $("#no_hp-me").val(contact['no_hp']);
                    $("#email-me").val(contact['email']);

                    $("#nama-md").html(contact['nama']);
                    $("#email-md").html(contact['email']);
                    $("#no_hp-md").html(contact['no_hp']);

                    if (contact['perusahaan']) {
                        $("#perusahaan-me").append('<option value="' + contact['perusahaans'] + '" selected>' + contact['perusahaan']['nama'] + '</option>');
                        $("#perusahaan-md").html(contact['perusahaan']['nama']);
                    } else {
                        $("#perusahaan-me").append('<option value="" selected>' + contact['perusahaans'] + '</option>');
                        $("#perusahaan-md").html(contact['perusahaans']);
                    }
                }
            }
        });
    }

    $(document).ready(function() {
        $("#contact-table").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: pwd + 'json',
            columns: [
                { data: 'id',
                render:
                    function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'nama', name: 'nama' },
                { data: 'email', name: 'email'},
                { data: 'tgl_lahir', name:'tgl_lahir'},
                { data: 'perusahaans', name: 'perusahaans'},
                { data: 'no_hp', name: 'no_hp'},
                { data: 'aksi', name:'aksi', className: 'no-wrap'},
            ]
        });

        $('#perusahaan-me').css('width', '100%');
        $('#perusahaan-me').select2({
            placeholder: "Pilih Perusahaan",
            minimumInputLength: 3,
            ajax: {
                url: '{{ route("api.perusahaan.search") }}',
                dataType: 'json',
            },
        });

        $('#tgl_lahir-me').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "bottom left"
        });

        $("button.close").click(function () {
            $("#form-me").attr("action", "#");
            $("#form-md").attr("action", "#");
        });
    });
</script>
@stop
