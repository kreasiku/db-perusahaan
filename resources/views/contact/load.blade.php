<div class="card">
    <div class="card-header">
        <h3 class="card-title">History Catatan</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Catatan</th>
                    <th>User</th>
                    <th>Tanggal</th>
                </tr>
            </thead>
            @php
            $num=0;
            @endphp
            @foreach($catatan as $note)
            <tbody>
                <tr>
                    <td class="text-center">{{ $num += 1 }}</td>

                    @if($note->cp == $contact['id'])
                    <td class="word-wrap">{{ $note->catatans }}</td>
                    @endif

                    @if($note->user == Auth::user()->id)
                    <td class="no-wrap text-center">{{ Auth::user()->name }}</td>
                    @endif

                    <td class="no-wrap text-center">{{ $note->follow_up }}</td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>

    <div class="card-footer">
        <div class="card-tools">
            <div class="pagination-sm float-right">{{$catatan->links()}}</div>
        </div>
    </div>
</div>