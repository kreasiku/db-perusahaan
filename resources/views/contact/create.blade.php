@extends('adminlte::page')

@section('title', 'Manajemen Staff Perusahaan - Sistem Administrasi Internal PT KKU')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>Manajemen Staff Perusahaan</h1>
@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

    .word-wrap {
        word-break: break-all;
    }

    .no-wrap {
        white-space: nowrap;
    }

    .fixed {
        table-layout: fixed;
    }
</style>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Tambahkan Staff') }}</div>
                <form method="POST" id="form-create" action="store">
                    <div class="card-body">
                        @csrf
                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>

                                @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="" autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tgl_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Lahir') }}</label>

                            <div class="col-md-6">
                                <input id="tgl_lahir" type="text" class="form-control @error('tgl_lahir') is-invalid @enderror" name="tgl_lahir" value="{{ old('tgl_lahir') }}" autocomplete="tgl_lahir">

                                @error('tgl_lahir')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="perusahaans" class="col-md-4 col-form-label text-md-right">{{ __('Perusahaan') }}</label>

                            <div class="col-md-6 word-wrap">
                                <select id="perusahaans" type="text" class="js-example-basic-single form-control @error('perusahaans') is-invalid @enderror" name="perusahaans" required autocomplete="perusahaans">
                                </select>

                                @error('perusahaans')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="no_hp" class="col-md-4 col-form-label text-md-right">{{ __('Nomor') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control @error('no_hp') is-invalid @enderror" name="no_hp" required autocomplete="no_hp">

                                @error('no_hp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" hidden>
                            <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('User') }}</label>

                            <div class="col-md-6">
                                <input id="user" type="text" value="{{ Auth::user()->id }}" class="form-control @error('user') is-invalid @enderror" name="user" required autocomplete="user">

                                @error('user')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <button type="submit" id="send" class="btn btn-primary">
                                {{ __('Tambah') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('js')
<script src="/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function() {
        $("#form-create").submit(function () {
            $("#send").attr("disabled", true);
            return true;
        });

        $('#perusahaans').css('width', '100%');
        $('#perusahaans').select2({
            placeholder: 'Pilih perusahaan',
            minimumInputLength: 3,
            ajax: {
                url: '{{ route("api.perusahaan.search") }}',
                dataType: 'json',
            },
        });
        $('#tgl_lahir').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "top left"
        });
    });
</script>
@stop