@extends('adminlte::page')

@section('title', 'Manajemen Staff Perusahaan - Sistem Administrasi Internal PT KKU')

@php
config(['adminlte.logo' => Auth::user()->name]);
config(['adminlte.logo_img' => Auth::user()->avatar]);
@endphp

@section('content_header')
<h1>{{$contact->nama}} - 
    @if ($contact->perusahaans)
        {{$contact->perusahaan->nama}}
    @else
        {{$contact->perusahaan}}
    @endif
</h1>
@stop

@section('sidebar_collapse', 'true')

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
<style>
    img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

    th {
        text-align: center !important;
    }

    td {
        vertical-align: middle !important;
    }

    .word-wrap {
        word-break: break-all;
        /* word-wrap: break-word; */
        /* overflow-wrap: break-word; */
    }

    .no-wrap {
        white-space: nowrap;
    }

    .fixed {
        table-layout: fixed;
    }
</style>
@stop

@section('content')
<div class="row">
    <section class="col-lg-7 connectedSortable">
        <!-- Form Catatan -->
        <form method="POST" id="form-create" action="{{ route('catatan.store') }}">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Form Catatan</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-body">
                        @csrf

                        <div class="form-group row">
                            <label for="cp" class="col-md-4 col-form-label text-md-right">{{ __('Contact Person') }}</label>

                            <div class="col-md-6">
                                
                                
                                <input readonly type="text" class="form-control" value="{{ $contact['nama'] }} - @if($contact->perusahaans) {{$contact->perusahaan->nama}} @else {{$contact->perusahaan}} @endif">
                                <input readonly id="perusahaan" type="hidden" class="form-control @error('perusahaans') is-invalid @enderror" name="perusahaans" required autocomplete="perusahaans" Peru autofocus value="{{$contact->perusahaans}}">
                                <input readonly id="cp" type="hidden" class="form-control @error('cp') is-invalid @enderror" name="cp" required autocomplete="cp" autofocus value="{{ $contact->id }}">

                                @error('cp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="catatans" class="col-md-4 col-form-label text-md-right">{{ __('Catatan') }}</label>

                            <div class="col-md-6">
                                <textarea id="catatans" rows="5" class="form-control @error('catatans') is-invalid @enderror" name="catatans" required autocomplete="catatans" autofocus></textarea>

                                @error('catatans')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                            <div class="col-md-6">
                                <select name="status" id="status" required class="form-control @error('status') is-invalid @enderror" autocomplete="status" autofocus>
                                    @foreach ($sc as $s)
                                    <option value="{{ $s->id }}">{{ $s->status }}</option>
                                    @endforeach
                                </select>

                                @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="follow_up" class="col-md-4 col-form-label text-md-right">{{ __('Follow Up') }}</label>

                            <div class="col-md-6">
                                <input id="follow_up" type="text" value="" class="form-control @error('follow_up') is-invalid @enderror" name="follow_up" autocomplete="follow_up">

                                @error('follow_up')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" hidden>
                            <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('User') }}</label>

                            <div class="col-md-6">
                                <input id="user" type="text" value="{{ Auth::user()->id }}" class="form-control @error('user') is-invalid @enderror" name="user" required autocomplete="user">

                                @error('user')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-right">
                        <button type="submit" id="send" class="btn btn-primary">
                            {{ __('Tambah') }}
                        </button>
                    </div>
                </div>
        </form>
</div>
<!-- End of Form -->

<div class="card">
    <div class="card-header">
        <h3 class="card-title">History Catatan</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-hover dataTables" id="contact-data">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Catatan</th>
                    <th>User</th>
                    <th>Tanggal</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

</section>

<!-- Detail CP -->
<section class="col-lg-5 connectedSortable">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Detail Staff</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Staff') }}</label>

                <div class="col-md-6">
                    <p class="col-form-label">{{$contact['nama']}}</p>
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email Staff') }}</label>

                <div class="col-md-6">
                    <p class="col-form-label">{{$contact['email']}}</p>
                </div>
            </div>

            <div class="form-group row">
                <label for="no_hp" class="col-md-4 col-form-label text-md-right">{{ __('Nomor HP') }}</label>

                <div class="col-md-6">
                    <p class="col-form-label">{{$contact['no_hp']}}</p>
                </div>
            </div>

            @if ($contact->perusahaans)
            <div class="form-group row">
                <label for="perusahaan" class="col-md-4 col-form-label text-md-right">{{ __('Asal Perusahaan') }}</label>

                <div class="col-md-6">
                    <p class="col-form-label">{{$contact->perusahaan->nama}}</p>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="deskripsi" class="col-md-4 col-form-label text-md-right">{{ __('Deskripsi Perusahaan') }}</label>

                <div class="col-md-6">
                    <p class="col-form-label">{{$contact->perusahaan->deskripsi}}</p>
                </div>
            </div>
            @else
            <div class="form-group row">
                <label for="perusahaan" class="col-md-4 col-form-label text-md-right">{{ __('Asal Perusahaan') }}</label>

                <div class="col-md-6">
                    <p class="col-form-label">{{$contact->perusahaan}}</p>
                </div>
            </div>
            @endif
        </div>
        <div class="card-footer"></div>
    </div>
</section>
</div>
@stop

@section('js')
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="/js/dataTables-num-html.js"></script>
<script>
    var stat = "#status";
    var p = "#perusahaan";
    var cp = "#cp";
    var total_cat = '{{ \App\Catatan::count() }}';
    var pwd = window.location.pathname;
    pwd = pwd.substring(0);
    console.log(pwd);
    if (pwd.length > 1) {
        pwd += "/";
    } else {
        pwd += "contact/";
    }
    console.log(pwd);

    $(document).ready(function() {
        $("#form-create").submit(function () {
            $("#send").attr("disabled", true);
            return true;
        });
        $('#contact-data').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": false,
            "lengthMenu": [5, 10, 25, 50, 100],
            ajax: pwd + 'json',
            columnDefs: [
                {
                    targets: [0,2,3],
                    className: 'text-center text-nowrap'
                }
            ],
            columns: [{
                    data: 'created_at',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    data: 'catatans',
                    name: 'catatans',
                    className: 'text-wrap'
                },
                {
                    data: 'user.name',
                    name: 'user',
                },
                {
                    data: 'follow_up',
                    name: 'follow_up',
                }
            ]
        });

        $(stat).css('width', '100%');
        $(p).css('width', '100%');
        $(cp).css('width', '100%');
        $(stat).select2({
            placeholder: 'Pilih status'
        });

        for (let x = 1; x <= total_cat; x++) {
            $(p.concat("-" + x)).css('width', '100%');
            $(p.concat("-" + x)).select2();
        }

        $('#follow_up').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "bottom left"
        });
    });
</script>
@stop