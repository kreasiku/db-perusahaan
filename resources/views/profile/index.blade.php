@extends('adminlte::page')

@section('title', 'Profile')

@php
$user = Auth::user();
config(['adminlte.logo' => $user->name]);
config(['adminlte.logo_img' => $user->avatar]);
@endphp

@section('content_header')
<h1>Profile</h1>
@stop

@section('sidebar')

@section('css')
<style>
	img {
        vertical-align: middle;
        border-style: none;
        width: 33px !important;
        height: 33px;
    }

	img.cropped {
		width: 100px !important;
		height: 100px;
	}

	th {
        text-align: center !important;
    }

    td {
        vertical-align: middle !important;
    }

    .word-wrap {
        word-break: break-all;
        /* word-wrap: break-word; */
        /* overflow-wrap: break-word; */
    }

    .no-wrap {
        white-space: nowrap;
    }

    .fixed {
        table-layout: fixed;
    }

	.fc-today {
		background: ghostwhite !important;
	}
</style>
@stop

@section('content')
<!-- Main content -->
<div class="container-fluid">
	<!-- Message -->
	@if ($message = Session::get('success'))
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong>{{ $message }}</strong>
	</div>
	@elseif ($message = Session::get('failed'))
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong>{{ $message }}</strong>
	</div>
	@endif
	<!-- Message -->
	<div class="row justify-content-center">
		<div class="col-md-4">
			<!-- Profile Image -->
			@php
			$c_selesai = \App\Catatan::where('user','=',$user->id)->where('status','=',3)->count();
			$c_proses = \App\Catatan::where('user','=',$user->id)->where('status','=',1)->count();
			$c_gagal = \App\Catatan::where('user','=',$user->id)->where('status','=',2)->count();
			@endphp
			<div class="card card-primary card-outline">
				<div class="card-body box-profile">
					<div class="text-center">
						<img class="profile-user-img img-fluid cropped img-circle" src="{{ $user->avatar }}" alt="User profile picture">
					</div>

					<h3 class="profile-username text-center">{{ $user->name }}</h3>

					<p class="text-muted text-center">{{ $user->divisi }} at<br>PT. Kusuma Kreasi Utama</p>
					<ul class="list-group list-group-unbordered">
						<li class="list-group-item">
							<b>Pekerjaan <span class="badge badge-success">Selesai</span></b> <a class="float-right">{{ $c_selesai }}</a>
						</li>
						<li class="list-group-item">
							<b>Pekerjaan <span class="badge badge-info">Sedang Diproses</span></b> <a class="float-right">{{ $c_proses }}</a>
						</li>
						<li class="list-group-item">
							<b>Pekerjaan <span class="badge badge-danger">Gagal</span></b> <a class="float-right">{{ $c_gagal }}</a>
						</li>
					</ul>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
		<div class="col-md-8">
			<!-- About Me Box -->
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title">Profile Detail</h3>
				</div>
				<!-- /.card-header -->
				<div class="card-body mb-3">
					<strong><i class="fas fa-user mr-1"></i> Nama</strong>

					<p class="text-muted">
						{{ $user->name }}
					</p>

					<hr>

					<strong><i class="fas fa-envelope mr-1"></i> Email</strong>

					<p class="text-muted">{{ $user->email }}</p>

					<hr>

					<strong><i class="fas fa-briefcase mr-1"></i> Divisi</strong>

					<p class="text-muted">
						IT
					</p>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" class="btn btn-primary float-right" data-toggle="modal"
						data-target="#modal-edit-{{$user->id}}" href="#">Edit</button>
				</div>
				<!-- Modal Edit -->
				<div class="modal fade" id="modal-edit-{{$user->id}}">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header bg-info">
								<h4 class="modal-title">Edit Profile</h4>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form method="POST" action="{{ route('profile.update', $user->id) }}" enctype="multipart/form-data">
								@csrf
								@method('PUT')
								<div class="modal-body">
									<div class="form-group row">
										<label for="avatar" class="col-md-4 col-form-label text-md-right mt-5">{{ __('Photos') }}</label>

										<div class="col-md-6">
											<img id="image" class="mb-3 cropped" src="{{$user->avatar}}" height="100" width="100" alt="profile.jpg"><br>
											<input id="avatar" type="file" class="form-control border-0 @error('avatar') is-invalid @enderror" name="avatar" autocomplete="avatar" autofocus>

											@error('avatar')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>

									<div class="form-group row">
										<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>

										<div class="col-md-6">
											<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

											@error('name')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>

									<div class="form-group row">
										<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

										<div class="col-md-6">
											<input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="divisi">

											@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>

									<div class="form-group row">
										<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

										<div class="col-md-6">
											<input id="password" type="text" class="form-control @error('deskripsi') is-invalid @enderror" name="password" value="" placeholder="Masukkan Password Baru" autocomplete="password">

											@error('deskripsi')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>

									<div class="form-group row">
										<label for="divisi" class="col-md-4 col-form-label text-md-right">{{ __('Divisi') }}</label>

										<div class="col-md-6">
											<input id="divisi" type="text" class="form-control @error('divisi') is-invalid @enderror" name="divisi" value="{{ $user->divisi }}" required autocomplete="divisi">

											@error('divisi')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
								</div>
								<!-- Model Footer -->
								<div class="modal-footer justify-content-between">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-success">Simpan</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- End of Modal Edit -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</div><!-- /.container-fluid -->
@stop

@section('js')
<script>
	function display(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(event) {
				$('#image').attr('src', event.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).ready(function() {
		$("#avatar").change(function () {
			display(this);
		});
	});
</script>
@stop
